// DialogView component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import {
  Dialog,
  DialogProps,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText
} from '@material-ui/core'

export type ModalDialogProps = Omit<DialogProps, 'open'> & {
  open?: boolean
  error?: React.ReactNode
  actions: React.ReactNode
}

export default ({
  className,
  title,
  children,
  error,
  actions,
  open = true,
  fullWidth = true,
  maxWidth = 'sm'
}: ModalDialogProps) => (
  <Dialog className={className} open={open} fullWidth={fullWidth} maxWidth={maxWidth}>
    <DialogTitle>{title}</DialogTitle>
    <DialogContent>{children}</DialogContent>
    {error && (
      <DialogContent>
        <DialogContentText variant="body2" color="error">
          {error}
        </DialogContentText>
      </DialogContent>
    )}
    <DialogActions>{actions}</DialogActions>
  </Dialog>
)
