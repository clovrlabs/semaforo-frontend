// SectionView component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Typography, Box, BoxProps } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: theme.spacing(3)
  }
}))

export type SectionPageProps = Omit<BoxProps, 'title'> & {
  title: React.ReactNode
}

export default ({ title, children, ...rest }: SectionPageProps) => {
  const classes = useStyles()

  return (
    <Box {...rest}>
      <Typography variant="h4" className={classes.title}>
        {title}
      </Typography>
      {children}
    </Box>
  )
}
