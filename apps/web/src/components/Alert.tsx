import React from 'react'
import { Snackbar } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import Alert, { AlertProps } from '@material-ui/lab/Alert'

const useStyles = makeStyles(theme => ({
  root: {
    bottom: theme.spacing(8)
  }
}))

export type ErrorMessageProps = Pick<AlertProps, 'severity'> & {
  open: boolean
  message: string
}

export default ({ open: openParam, message, severity }: ErrorMessageProps) => {
  const classes = useStyles()
  const [open, setOpen] = React.useState(false)
  const onClose = () => {
    setOpen(false)
  }

  React.useEffect(() => {
    setOpen(openParam)
  }, [openParam])

  return (
    <Snackbar className={classes.root} open={open} onClose={onClose}>
      <Alert onClose={onClose} severity={severity} variant="filled">
        {message}
      </Alert>
    </Snackbar>
  )
}
