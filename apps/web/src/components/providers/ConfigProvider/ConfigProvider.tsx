import React from 'react'
import TOML from 'toml'
import axios from 'axios'
import { context } from './context'

export type ConfigProviderProps = {
  path: string
  children: React.ReactNode
}

export default ({ path, children }: ConfigProviderProps) => {
  const [config, setConfig] = React.useState<any>()

  React.useEffect(() => {
    axios.get(path).then(res => setConfig(TOML.parse(res.data)))
  }, [path])

  return (
    <context.Provider value={config}>{config ? children : 'Loading config...'}</context.Provider>
  )
}
