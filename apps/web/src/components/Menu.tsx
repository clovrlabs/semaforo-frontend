// Application menu.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useLocation } from 'react-router-dom'
import _ from 'lodash'
import {
  Dns as DnsIcon,
  People as UserIcon,
  Settings as SettingsIcon,
  Subject as TemplateIcon,
  LockOpen as LockOpenIcon,
  PowerSettingsNew as RemoveAccountIcon,
  LocalOffer as TagIcon,
  Extension as IntegrationIcon
} from '@material-ui/icons'
import { Path } from 'src/settings'
import { replace } from 'src/lib/history'
import { appContext } from 'src/lib/context'
import { isAdmin } from 'src/lib/auth'
import { concat } from 'src/lib/utils'
import { TagsIcon } from 'src/components/icons'
import { Tree, TreeItem } from '@semaforo/ui'

export default () => {
  const history = useHistory()
  const location = useLocation()
  const { token, tags, toggleMenu } = React.useContext(appContext)
  const { toggleRemoveAccountDialog } = React.useContext(appContext)
  const { t } = useTranslation()
  const [path, setPath] = React.useState<string>()

  const goto = (path: string) => {
    if (path == '/settings/remove-account') {
      toggleRemoveAccountDialog()
    } else if (path) {
      history.push(path)
    }
  }

  const menuItems = React.useMemo((): TreeItem[] => {
    const admin = isAdmin(token)

    return [
      {
        id: replace(Path.Domains, { tagId: 0 }),
        name: t('menu.domains'),
        items: tags.map(({ id, name }) => ({ id: `/tags/${id}/domains`, name, icon: <TagIcon /> })),
        icon: <DnsIcon />
      },
      ...(admin
        ? [
            {
              id: Path.Tags,
              name: t('menu.tags'),
              icon: <TagsIcon />
            },
            {
              id: Path.Users,
              name: t('menu.users'),
              icon: <UserIcon />
            }
          ]
        : []),
      {
        id: '/settings',
        name: t('menu.settings'),
        selectable: false,
        icon: <SettingsIcon />,
        divider: 'top',
        items: [
          ...(admin
            ? [
                {
                  id: Path.Integrations,
                  name: t('menu.integrations'),
                  icon: <IntegrationIcon />
                },
                {
                  id: Path.Templates,
                  name: t('menu.templates'),
                  icon: <TemplateIcon />
                }
              ]
            : []),
          {
            id: concat([_.trimEnd(location.pathname, '/api-key'), '/api-key']),
            name: t('menu.apiKey'),
            icon: <LockOpenIcon />
          },
          {
            id: Path.RemoveAccount,
            name: t('menu.removeAccount'),
            icon: <RemoveAccountIcon color="secondary" />
          }
        ]
      }
    ]
  }, [tags, path, location.pathname, token])

  React.useEffect(() => {
    const getItemIds = (items: TreeItem[]): string[] =>
      items.map(item => [...getItemIds(item.items || []), `${item.id}`]).flat()
    const ids = getItemIds(menuItems)
    const path = ids.find(id => location.pathname.includes(id))

    setPath(path)
  }, [location.pathname, menuItems])

  return (
    <Tree
      items={menuItems}
      selectedItemId={path}
      onSelectItem={item => {
        goto(`${item.id}`)
        toggleMenu()
      }}
    />
  )
}
