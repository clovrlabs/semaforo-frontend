// Tags icon.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { SvgIcon, SvgIconProps } from '@material-ui/core'

export default (props: SvgIconProps) => (
  <SvgIcon viewBox="0 0 566.93 566.93" {...props}>
    <g>
      <path
        fill="none"
        stroke="#BE1622"
        strokeWidth="7.9855"
        strokeMiterlimit="10"
        d="M343.661,125.607
		c0-30.314,23.636-54.893,52.794-54.893h14.696c29.165,0,52.804,24.579,52.804,54.893"
      />
      <path
        fill="none"
        stroke="#BE1622"
        strokeWidth="7.9855"
        strokeMiterlimit="10"
        d="M343.661,435.337v40.403
		c0,30.324,23.64,54.9,52.804,54.9h14.694c29.161,0,52.796-24.576,52.796-54.9V125.607"
      />
    </g>
    <line
      fill="none"
      stroke="#BE1622"
      strokeWidth="9.035"
      strokeLinecap="round"
      strokeMiterlimit="10"
      x1="343.041"
      y1="435.665"
      x2="310.85"
      y2="435.665"
    />
    <path
      d="M315.211,487.173c-0.412,32.827-33.123,59.364-73.472,59.364h-20.343c-40.35,0-73.062-26.537-73.062-59.278V109.107
	c0-32.74,32.713-59.282,73.062-59.282h20.343c40.35,0,73.06,26.542,73.472,59.2V487.173z"
    />
    <path
      fill="#FFFFFF"
      stroke="#000000"
      strokeWidth="7.7609"
      strokeMiterlimit="10"
      d="M283.364,487.251
	c0,32.745-32.713,59.286-73.064,59.286h-20.334c-40.357,0-73.069-26.541-73.069-59.286V109.107c0-32.74,32.712-59.282,73.069-59.282
	H210.3c40.352,0,73.064,26.542,73.064,59.282V487.251z"
    />
    <g>
      <path
        fill="#BE1622"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M33.103,114.429l149.762,76.778
		c24.131,12.369,53.736,2.831,66.12-21.315c12.369-24.138,2.834-53.743-21.306-66.12L77.918,27"
      />
      <path
        fill="none"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M254.396,147.499
		c0,27.129-21.985,49.124-49.12,49.124c-27.118,0-49.113-21.994-49.113-49.124c0-27.125,21.995-49.117,49.113-49.117
		C232.41,98.382,254.396,120.374,254.396,147.499z"
      />
      <g>
        <path
          d="M226.254,103.132c5.291,7.838,8.384,17.284,8.384,27.454c0,27.126-21.989,49.113-49.128,49.113
			c-7.504,0-14.598-1.736-20.972-4.752c8.829,13.074,23.774,21.676,40.737,21.676c27.134,0,49.12-21.994,49.12-49.124
			C254.396,127.879,242.875,110.997,226.254,103.132z"
        />
      </g>
      <path
        fill="#BE1622"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M77.918,27
		c24.14,12.375,33.689,41.988,21.306,66.124c-12.369,24.146-41.981,33.682-66.121,21.305C8.965,102.053-0.57,72.447,11.799,48.308
		C24.183,24.168,53.771,14.623,77.918,27z"
      />
    </g>
    <g>
      <path
        fill="#F9B233"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M33.103,263.455l149.762,76.782
		c24.132,12.368,53.736,2.826,66.12-21.317c12.369-24.14,2.835-53.742-21.306-66.121L77.918,176.031"
      />
      <path
        fill="#F9B233"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M254.396,296.531
		c0,27.127-21.987,49.118-49.119,49.118c-27.119,0-49.114-21.991-49.114-49.118c0-27.126,21.995-49.121,49.114-49.121
		C232.408,247.41,254.396,269.405,254.396,296.531z"
      />
      <g>
        <path
          d="M226.254,252.163c5.29,7.833,8.382,17.281,8.382,27.45c0,27.126-21.986,49.109-49.127,49.109
			c-7.511,0-14.597-1.728-20.97-4.749c8.829,13.074,23.775,21.676,40.737,21.676c27.131,0,49.119-21.991,49.119-49.118
			C254.396,276.907,242.883,260.027,226.254,252.163z"
        />
      </g>
      <path
        fill="#F9B233"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M77.918,176.031
		c24.14,12.376,33.688,41.988,21.306,66.123c-12.369,24.14-41.982,33.677-66.121,21.301c-24.14-12.376-33.673-41.982-21.304-66.12
		C24.184,173.195,53.772,163.653,77.918,176.031z"
      />
    </g>
    <g>
      <path
        fill="#00983A"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M33.103,423.745l149.762,76.774
		c24.132,12.372,53.736,2.835,66.12-21.309c12.369-24.144,2.835-53.75-21.306-66.126L77.918,336.314"
      />
      <path
        fill="#00983A"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M254.396,456.813
		c0,27.127-21.987,49.122-49.119,49.122c-27.119,0-49.114-21.995-49.114-49.122c0-27.126,21.995-49.113,49.114-49.113
		C232.408,407.7,254.396,429.688,254.396,456.813z"
      />
      <g>
        <path
          d="M226.254,412.445c5.29,7.837,8.382,17.285,8.382,27.454c0,27.126-21.986,49.113-49.127,49.113
			c-7.511,0-14.597-1.735-20.97-4.753c8.829,13.074,23.775,21.676,40.737,21.676c27.131,0,49.119-21.995,49.119-49.122
			C254.396,437.193,242.876,420.31,226.254,412.445z"
        />
      </g>
      <path
        fill="#00983A"
        stroke="#000000"
        strokeWidth="7.7609"
        strokeMiterlimit="10"
        d="M77.918,336.314
		c24.14,12.38,33.688,41.986,21.306,66.126c-12.369,24.144-41.982,33.677-66.121,21.305c-24.14-12.38-33.673-41.982-21.304-66.122
		C24.184,333.483,53.779,323.938,77.918,336.314z"
      />
    </g>
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M381.813,168.747l3.794-6.534
	c0.839-1.478,2.632-1.478,3.586-0.738c0.526,0.316,9.06,6.532,15.91,6.532c5.482,0,9.592-3.583,9.592-8.115
	c0-5.374-4.531-9.062-13.386-12.647c-9.904-4.003-19.815-10.328-19.815-22.766c0-9.377,6.96-20.234,23.717-20.234
	c10.752,0,18.971,5.481,21.08,7.062c1.053,0.632,1.369,2.422,0.63,3.478l-4.002,6.006c-0.847,1.266-2.426,2.108-3.69,1.266
	c-0.843-0.525-8.854-5.795-14.651-5.795c-6.007,0-9.272,4.002-9.272,7.374c0,4.957,3.899,8.328,12.433,11.805
	c10.226,4.113,22.028,10.225,22.028,23.82c0,10.857-9.378,20.869-24.241,20.869c-13.278,0-21.075-6.219-23.185-8.219
	C381.389,170.959,380.862,170.433,381.813,168.747z"
    />
    <path
      stroke="#000000"
      strokeWidth="2.9103"
      strokeMiterlimit="10"
      d="M302.355,210.081c0-1.053,0.842-2.004,2-2.004h122.868
	c1.158,0,2,0.952,2,2.004v8.645c0,1.054-0.842,2.004-2,2.004H310.439v17.283h116.675c1.053,0,2.004,0.948,2.004,2.001v8.75
	c0,1.16-0.951,2.004-2.004,2.004H310.439v18.439h116.784c1.158,0,2,0.952,2,2.004v8.641c0,1.057-0.842,2.004-2,2.004H304.355
	c-1.158,0-2-0.947-2-2.004V210.081z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M376.858,311.551c0.104-0.846,0.736-1.587,1.896-1.587
	h1.688c0.84,0,1.472,0.425,1.792,1.057l23.184,49.749c0.211,0,0.211,0,0.318,0l23.19-49.749c0.312-0.632,0.838-1.057,1.785-1.057
	h1.688c1.158,0,1.79,0.741,1.896,1.587l12.438,70.824c0.314,1.47-0.528,2.422-1.898,2.422h-9.592c-0.948,0-1.788-0.843-1.999-1.579
	l-6.221-39.947c-0.105,0-0.313,0-0.313,0l-18.449,41.421c-0.209,0.636-0.84,1.162-1.788,1.162h-1.898
	c-0.947,0-1.474-0.526-1.794-1.162l-18.653-41.421c0,0-0.209,0-0.315,0l-6.11,39.947c-0.109,0.736-0.953,1.579-1.9,1.579h-9.485
	c-1.372,0-2.213-0.952-2-2.422L376.858,311.551z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M370.605,486.179l32.778-70.934
	c0.314-0.632,0.841-1.158,1.794-1.158h1.053c1.053,0,1.474,0.526,1.787,1.158l32.467,70.934c0.632,1.368-0.215,2.736-1.794,2.736
	h-9.171c-1.581,0-2.319-0.627-3.055-2.109l-5.164-11.377h-31.516l-5.164,11.377c-0.421,1.057-1.372,2.109-3.053,2.109h-9.171
	C370.816,488.915,369.972,487.547,370.605,486.179z M416.136,463.73l-10.537-23.191h-0.319l-10.327,23.191H416.136z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M501.911,107.3c0-1.052,0.841-2.001,2.001-2.001h42.896
	c1.162,0,2.006,0.949,2.006,2.001v8.644c0,1.054-0.844,2.003-2.006,2.003h-31.195v19.813h26.03c1.053,0,2.005,0.95,2.005,2.003
	v8.642c0,1.054-0.952,2.002-2.005,2.002h-26.03v26.666c0,1.054-0.951,2.003-2.004,2.003h-9.697c-1.16,0-2.001-0.949-2.001-2.003
	V107.3z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M525.311,205.769c21.076,0,37.942,16.971,37.942,38.05
	s-16.866,37.837-37.942,37.837c-21.077,0-37.838-16.759-37.838-37.837S504.233,205.769,525.311,205.769z M525.311,267.955
	c13.278,0,24.241-10.855,24.241-24.136s-10.963-24.347-24.241-24.347c-13.277,0-24.134,11.066-24.134,24.347
	S512.033,267.955,525.311,267.955z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M497.798,313.021c0-1.053,0.845-2,2.002-2h30.146
	c12.646,0,22.976,10.118,22.976,22.662c0,9.693-6.432,17.597-15.599,21.285l14.438,26.775c0.741,1.365,0,3.054-1.793,3.054h-11.064
	c-0.953,0-1.478-0.526-1.686-0.952l-14.018-27.926h-11.699v26.877c0,1.049-0.95,2.001-2.005,2.001H499.8
	c-1.157,0-2.002-0.952-2.002-2.001V313.021z M528.786,344.639c5.586,0,10.433-4.846,10.433-10.745
	c0-5.588-4.847-10.224-10.433-10.224h-17.182v20.969H528.786z"
    />
    <path
      stroke="#000000"
      strokeWidth="1.9402"
      strokeMiterlimit="10"
      d="M525.307,413.557c21.08,0,37.946,16.969,37.946,38.052
	c0,21.079-16.866,37.837-37.946,37.837c-21.079,0-37.838-16.758-37.838-37.837C487.469,430.525,504.228,413.557,525.307,413.557z
	 M525.307,475.74c13.28,0,24.241-10.852,24.241-24.132s-10.961-24.351-24.241-24.351c-13.279,0-24.134,11.07-24.134,24.351
	S512.027,475.74,525.307,475.74z"
    />
  </SvgIcon>
)
