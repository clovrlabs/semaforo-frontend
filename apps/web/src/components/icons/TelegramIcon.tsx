// Dead icon.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { SvgIcon, SvgIconProps } from '@material-ui/core'

export default (props: SvgIconProps) => (
  <SvgIcon viewBox="0 0 512 512" {...props}>
    <g>
      <path
        d="M479.7,101.7l-67.9,320.3c-5.1,22.6-18.5,28.2-37.5,17.6l-103.5-76.2l-49.9,48c-5.5,5.5-10.1,10.1-20.8,10.1l7.4-105.4
		l191.8-173.3c8.3-7.4-1.8-11.6-13-4.1L149.3,287.9L47.3,256c-22.2-6.9-22.6-22.2,4.6-32.8L451.1,69.3
		C469.6,62.4,485.7,73.4,479.7,101.7z"
      />
    </g>
  </SvgIcon>
)
