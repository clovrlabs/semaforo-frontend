// Entry point.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

export { default as LogoIcon } from './LogoIcon'
export { default as EmailMultipleIcon } from './EmailMultipleIcon'
export { default as ExtensionIcon } from './ExtensionIcon'
export { default as GoogleIcon } from './GoogleIcon'
export { default as FolderOpenIcon } from './FolderOpenIcon'
export { default as FileIcon } from './FileIcon'
export { default as TagsIcon } from './TagsIcon'
export { default as DeadIcon } from './DeadIcon'
export { default as YoutubeIcon } from './YoutubeIcon'
export { default as TelegramIcon } from './TelegramIcon'
export { default as ImportIcon } from './ImportIcon'
