// Protected route.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Route, Redirect, RouteProps } from 'react-router-dom'
import { parseToken } from 'src/lib/auth'

export type ProtectedRouteProps = Omit<RouteProps, 'render'> & {
  token: string
  adminAccess?: boolean
}

export default ({ component: Component, token, adminAccess, ...rest }: ProtectedRouteProps) => {
  if (!Component) return null

  const tokenInfo = parseToken(token)
  const admin = tokenInfo && tokenInfo.admin

  return (
    <Route
      {...rest}
      render={props => {
        const referrer = props.location.pathname

        if (token) {
          return !adminAccess || admin ? <Component {...props} /> : <Redirect to="/" />
        }

        return <Redirect to={{ pathname: '/', state: { referrer } }} />
      }}
    />
  )
}
