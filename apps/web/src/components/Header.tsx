// Application header.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { Link } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { Menu as MenuIcon, ExitToApp as ExitToAppIcon } from '@material-ui/icons'
import {
  AppBar,
  AppBarProps,
  Toolbar,
  IconButton,
  Typography,
  Button,
  Hidden,
  LinearProgress,
  Tooltip,
  Divider
} from '@material-ui/core'
import { SubmitButton } from '@semaforo/ui/buttons'
import { Path } from 'src/settings'
import { parseToken } from 'src/lib/auth'
import { appContext } from 'src/lib/context'
import { usePush } from 'src/lib/history'
import { DialogView } from 'src/components/views'
import { LogoIcon, ExtensionIcon, YoutubeIcon, TelegramIcon } from 'src/components/icons'

export type HeaderProps = AppBarProps & {
  showProgressBar: boolean
}

const useStyles = makeStyles(theme => ({
  toolbar: {
    paddingRight: theme.spacing(1)
  },
  logo: {
    padding: 0,
    width: theme.spacing(8),
    height: theme.spacing(8),
    margin: '-3px 12px 0 -5px',
    [theme.breakpoints.down('md')]: {
      marginLeft: theme.spacing(0.5),
      width: theme.spacing(7),
      height: theme.spacing(7)
    }
  },
  logoIcon: {
    color: 'black',
    width: '100%',
    height: '100%'
  },
  title: {
    flexGrow: 1
  },
  subtitle: {
    fontWeight: 'normal'
  },
  link: {
    textDecoration: 'none',
    color: 'inherit'
  },
  divider: {
    height: 30
  }
}))

const Header = ({ showProgressBar, position = 'static', ...rest }: HeaderProps) => {
  const classes = useStyles()
  const push = usePush()
  const { config, token, logout, toggleMenu } = React.useContext(appContext)
  const tokenInfo = parseToken(token)
  const { t } = useTranslation()
  const [confirmDialogOpen, setConfirmDialogOpen] = React.useState(false)
  const { links } = config

  const extension = React.useMemo(() => {
    const { userAgent } = window.navigator

    return userAgent.includes('Firefox') ? config.extensions.firefox : config.extensions.chrome
  }, [window.navigator.userAgent])

  const onLogout = () => {
    setConfirmDialogOpen(false)
    logout()
  }

  return (
    <AppBar position={position} {...rest}>
      <Toolbar disableGutters className={classes.toolbar}>
        {/* Mobile version */}
        <Hidden mdUp>
          <IconButton color="inherit" onClick={toggleMenu}>
            <MenuIcon />
          </IconButton>
        </Hidden>
        {/* Desktop version */}
        <Hidden smDown>
          <IconButton color="inherit" className={classes.logo} onClick={() => push(Path.Home)}>
            <LogoIcon fontSize="large" className={classes.logoIcon} />
          </IconButton>
        </Hidden>
        <div className={classes.title}>
          <Link to="/" className={classes.link}>
            {tokenInfo?.username && (
              <Typography className={classes.subtitle} variant="subtitle1">
                {t('header.subtitle', { username: tokenInfo.username })}
              </Typography>
            )}
          </Link>
        </div>
        {links?.quickstarts && (
          <Tooltip title={t<string>('header.tooltips.quickStarts')}>
            <IconButton color="inherit" href={links?.quickstarts} target="_blank">
              <YoutubeIcon />
            </IconButton>
          </Tooltip>
        )}
        {links?.communityAnnouncements && (
          <Tooltip title={t<string>('header.tooltips.communityAnnouncements')}>
            <IconButton color="inherit" href={links?.communityAnnouncements} target="_blank">
              <TelegramIcon />
            </IconButton>
          </Tooltip>
        )}
        <Tooltip title={t<string>('header.tooltips.browserExtension')}>
          <IconButton color="inherit" href={extension} target="_blank">
            <ExtensionIcon />
          </IconButton>
        </Tooltip>
        <Divider className={classes.divider} orientation="vertical" />
        <Tooltip title={t<string>('header.tooltips.logout')}>
          <IconButton color="inherit" onClick={() => setConfirmDialogOpen(true)}>
            <ExitToAppIcon />
          </IconButton>
        </Tooltip>
      </Toolbar>
      {/* CONFIRM DIALOG */}
      <DialogView
        maxWidth="xs"
        open={confirmDialogOpen}
        title={t('header.confirmDialogTitle')}
        actions={
          <>
            <Button onClick={() => setConfirmDialogOpen(false)}>{t('buttons.cancel')}</Button>
            <SubmitButton onClick={onLogout}>{t('buttons.continue')}</SubmitButton>
          </>
        }
      >
        {t('header.confirmDialogText')}
      </DialogView>
      {showProgressBar && <LinearProgress color="secondary" />}
    </AppBar>
  )
}

export default Header
