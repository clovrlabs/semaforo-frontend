// Remove Account dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { Button } from '@material-ui/core'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsyncFn } from 'src/lib/async'
import { removeAccount } from 'src/providers/misc'
import { DialogView } from 'src/components/views'

type Props = {
  open: boolean
}

export default ({ open }: Props) => {
  const { t } = useTranslation()
  const { http, logout, toggleRemoveAccountDialog } = React.useContext(appContext)

  const { fn: onSubmitState, ...submitState } = useAsyncFn(async () => {
    await removeAccount(http)
    toggleRemoveAccountDialog()
    logout()
  }, [http, logout, toggleRemoveAccountDialog])

  const { loading, error } = submitState

  return (
    <DialogView
      maxWidth="xs"
      open={open}
      title={t('dialogs.removeAccount.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button disabled={loading} onClick={toggleRemoveAccountDialog}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton color="secondary" loading={loading} onClick={onSubmitState}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      {t('dialogs.removeAccount.message')}
    </DialogView>
  )
}
