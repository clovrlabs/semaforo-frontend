// TemplateEditor component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import {
  Box,
  IconButton,
  Popover,
  Table,
  TableHead,
  TableBody,
  TableRow,
  TableCell
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { HelpOutline as HelpIcon } from '@material-ui/icons'
import { TextArea } from '@semaforo/ui/fields'
import settings from 'src/settings'

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative'
  },
  helpBtn: {
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: theme.zIndex.tooltip - 1
  },
  typography: {
    padding: theme.spacing(2)
  }
}))

export type TemplateEditorProps = {
  value: string
  onChange?: (value: string) => void
}

export default ({ value, onChange }: TemplateEditorProps) => {
  const classes = useStyles()
  const { t } = useTranslation()
  const [popoverEl, setPopoverEl] = React.useState<HTMLButtonElement | null>(null)

  return (
    <>
      <Box className={classes.container}>
        <IconButton
          className={classes.helpBtn}
          onClick={event => setPopoverEl(event.currentTarget)}
        >
          <HelpIcon />
        </IconButton>
        <TextArea
          value={value}
          buttonList={settings.textEditor.buttons}
          imageUploadSizeLimit={settings.textEditor.imageUploadSizeLimit}
          imageUploadUrl={settings.textEditor.imageUploadUrl}
          onChange={value => onChange && onChange(value)}
        />
      </Box>
      <Popover
        open={!!popoverEl}
        anchorEl={popoverEl}
        onClose={() => setPopoverEl(null)}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'right'
        }}
      >
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="center" colSpan={2}>
                {t('routes.templates.vars')}
              </TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>{`{{ url }}`}</TableCell>
              <TableCell>{t('routes.templates.urlVar')}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{`{{ api-key-url }}`}</TableCell>
              <TableCell>{t('routes.templates.apiKeyUrlVar')}</TableCell>
            </TableRow>
            <TableRow>
              <TableCell>{`{{ username }}`}</TableCell>
              <TableCell>{t('routes.templates.usernameVar')}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Popover>
    </>
  )
}
