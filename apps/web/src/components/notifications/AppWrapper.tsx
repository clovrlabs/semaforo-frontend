import React from 'react'
import { Box, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'

const useStyles = makeStyles(theme => ({
  center: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    height: '99vh'
  },
  content: {
    marginBottom: theme.spacing(1)
  },
  message: {
    fontSize: '1.25rem',
    fontFamily: 'monospace',
    whiteSpace: 'pre',
    textAlign: 'center'
  }
}))

export type LoadingProps = {
  message: string
  children: React.ReactNode
}

export default ({ message, children }: LoadingProps) => {
  const classes = useStyles()

  return (
    <Box className={classes.center}>
      <Box className={classes.content}>{children}</Box>
      <Typography className={classes.message}>{message}</Typography>
    </Box>
  )
}
