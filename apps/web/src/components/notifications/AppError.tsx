import React from 'react'
import { makeStyles } from '@material-ui/core/styles'
import { DeadIcon } from 'src/components/icons'
import AppWrapper from './AppWrapper'

const useStyles = makeStyles(theme => ({
  icon: {
    width: 100,
    height: 100,
    color: theme.palette.text.primary
  }
}))

export type LoadingProps = {
  message: string
}

export default ({ message }: LoadingProps) => {
  const classes = useStyles()

  return (
    <AppWrapper message={message}>
      <DeadIcon className={classes.icon} />
    </AppWrapper>
  )
}
