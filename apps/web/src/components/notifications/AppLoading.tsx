import React from 'react'
import { CircularProgress } from '@material-ui/core'
import AppWrapper from './AppWrapper'

export type LoadingProps = {
  message: string
}

export default ({ message }: LoadingProps) => (
  <AppWrapper message={message}>
    <CircularProgress size={60} />
  </AppWrapper>
)
