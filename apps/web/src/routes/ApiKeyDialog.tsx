// API Key dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useHistory } from 'react-router-dom'
import { useLastLocation } from 'react-router-last-location'
import { useTranslation } from 'react-i18next'
import { Button } from '@material-ui/core'
import { Refresh as RefreshIcon, FileCopyOutlined as CopyIcon } from '@material-ui/icons'
import { Alert } from '@semaforo/ui'
import { TextField } from '@semaforo/ui/fields'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { getApiKey, newApiKey } from 'src/providers/misc'
import { DialogView } from 'src/components/views'

export default () => {
  const history = useHistory()
  const lastLocation = useLastLocation()
  const { t } = useTranslation()
  const { http } = React.useContext(appContext)
  const [apiKey, setApiKey] = React.useState('')
  const [alert, setAlert] = React.useState<{ open: boolean; message?: string; autoHide?: boolean }>(
    { open: false }
  )

  const initState = useAsync(async () => {
    setApiKey(await getApiKey(http))
  })

  const { fn: onNewApiKey, ...newApiKeyState } = useAsyncFn(async () => {
    setApiKey(await newApiKey(http))
    setAlert({ open: true, autoHide: false, message: t('dialogs.apiKey.done') })
  }, [http])

  const onCopyToClipboard = () => {
    navigator.clipboard.writeText(apiKey)
    setAlert({ open: true, autoHide: true, message: t('dialogs.apiKey.copied') })
  }

  const loading = newApiKeyState.loading || initState.loading
  const error = newApiKeyState.error || initState.error

  return (
    <DialogView
      title={t('dialogs.apiKey.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button
            disabled={loading}
            title={t('dialogs.apiKey.newTokenTitle')}
            startIcon={<RefreshIcon />}
            onClick={onNewApiKey}
          >
            {t('dialogs.apiKey.newToken')}
          </Button>
          <Button
            disabled={loading}
            title={t('dialogs.apiKey.copyTitle')}
            startIcon={<CopyIcon />}
            onClick={onCopyToClipboard}
          >
            {t('dialogs.apiKey.copy')}
          </Button>
          <Button disabled={loading} onClick={() => history.push(lastLocation?.pathname || '/')}>
            {t('buttons.close')}
          </Button>
        </>
      }
    >
      <TextField
        placeholder={loading ? t('dialogs.apiKey.loading') : ''}
        multiline
        autoFocus
        required
        inputProps={{ spellCheck: false, readOnly: true }}
        value={apiKey}
      />
      <Alert
        open={alert.open}
        message={alert.message}
        autoHide={alert.autoHide}
        onClose={() => setAlert({ open: false })}
      />
    </DialogView>
  )
}
