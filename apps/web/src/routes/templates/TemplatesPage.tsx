// Templates page.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import {
  Refresh as RefreshIcon,
  Add as AddIcon,
  Edit as EditIcon,
  Delete as DeleteIcon
} from '@material-ui/icons'
import * as model from '@semaforo/model'
import { Alert } from '@semaforo/ui'
import DataTable, { RowAction, HeaderAction } from '@semaforo/ui/DataTable'
import settings, { Path } from 'src/settings'
import { appContext } from 'src/lib/context'
import { useAsyncRetry } from 'src/lib/async'
import { usePush } from 'src/lib/history'
import { concat } from 'src/lib/utils'
import { getTemplates } from 'src/providers/templates'
import SectionView from 'src/components/views/SectionView'
import RouteNotFound from 'src/routes/RouteNotFound'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'
import AddTemplateDialog from './AddTemplateDialog'
import EditTemplateDialog from './EditTemplateDialog'
import DeleteTemplateDialog from './DeleteTemplateDialog'

export default () => {
  const route = useRouteMatch()
  const { t } = useTranslation()
  const push = usePush()
  const { http, toggleProgressBar } = React.useContext(appContext)
  const [rows, setRows] = React.useState<model.Template[]>([])
  const [alert, setAlert] = React.useState<boolean | null>(null)

  const { loading, error, retry } = useAsyncRetry(async () => {
    setRows(await getTemplates(http))
  })

  const columns = [{ name: 'name', title: t('routes.templates.nameField') }]

  const headerActions: HeaderAction[] = [
    {
      icon: <AddIcon />,
      title: t('routes.templates.addItem'),
      onClick: () => push(Path.NewTemplate)
    },
    {
      icon: <RefreshIcon />,
      title: t('routes.templates.refresh'),
      onClick: () => retry()
    }
  ]

  const rowActions: RowAction[] = [
    {
      icon: <EditIcon />,
      title: t('routes.templates.editItem'),
      onClick: row => push(Path.EditTemplate, { id: row.id })
    },
    {
      icon: <DeleteIcon />,
      title: t('routes.templates.deleteItem'),
      color: 'secondary',
      onClick: row => push(Path.DeleteTemplate, { id: row.id })
    }
  ]

  const refresh = () => !loading && retry()

  React.useEffect(() => {
    setAlert(null)
    toggleProgressBar(loading)
    return () => toggleProgressBar(false)
  }, [loading])

  return (
    <SectionView title={t('routes.templates.title')}>
      <DataTable
        className="results"
        rowsPerPage={settings.rowsPerPage}
        rows={rows}
        columns={columns}
        headerActions={headerActions}
        rowActions={rowActions}
      />
      <Switch>
        <Route exact path={Path.Templates} />
        <Route path={Path.NewTemplate} render={() => <AddTemplateDialog onAccept={refresh} />} />
        <Route path={Path.EditTemplate} render={() => <EditTemplateDialog onAccept={refresh} />} />
        <Route path={concat([route.path, Path.ApiKey])} component={ApiKeyDialog} />
        <Route
          path={Path.DeleteTemplate}
          render={() => <DeleteTemplateDialog onAccept={refresh} />}
        />
        <Route render={() => <RouteNotFound />} />
      </Switch>
      <Alert
        autoHide={false}
        open={alert ?? (!loading && !!error)}
        message={t(error || 'errors.unknown')}
        onClose={() => setAlert(false)}
      />
    </SectionView>
  )
}
