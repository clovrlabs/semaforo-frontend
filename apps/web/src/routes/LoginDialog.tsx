// Login dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import { Button } from '@material-ui/core'
import { Dialog, DialogContent, DialogContentText, LinearProgress } from '@material-ui/core'
import { appContext } from 'src/lib/context'
import { parseToken } from 'src/lib/auth'
import { useAsyncFn } from 'src/lib/async'
import { Path } from 'src/settings'
import { GoogleIcon } from 'src/components/icons'

const useStyles = makeStyles(theme => ({
  dialogContent: {
    textAlign: 'center',
    marginBottom: theme.spacing(1)
  },
  error: {
    maxWidth: 300
  }
}))

export default () => {
  const history = useHistory()
  const classes = useStyles()
  const { t } = useTranslation()
  const { login } = React.useContext(appContext)
  const { loading, error, fn: asyncLogin } = useAsyncFn(login)

  const onClick = async () => {
    const { token } = await asyncLogin()
    const tokenInfo = parseToken(token)

    if (tokenInfo && !tokenInfo.admin) {
      history.push(Path.ApiKey)
    }
  }

  return (
    <Dialog open>
      <DialogContent className={classes.dialogContent}>
        <Button size="large" disabled={loading} startIcon={<GoogleIcon />} onClick={onClick}>
          {t('routes.login.signIn')}
        </Button>
        <LinearProgress hidden={!loading} />
      </DialogContent>
      {error && (
        <DialogContent className={classes.error}>
          <DialogContentText color="error">{t(error)}</DialogContentText>
        </DialogContent>
      )}
    </Dialog>
  )
}
