// Users page.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { DateTime } from 'luxon'
import { Typography, Link } from '@material-ui/core'
import { Refresh as RefreshIcon, Edit as EditIcon } from '@material-ui/icons'
import * as model from '@semaforo/model'
import { Alert } from '@semaforo/ui'
import DataTable, { Row } from '@semaforo/ui/DataTable'
import settings, { Path } from 'src/settings'
import { EmailMultipleIcon } from 'src/components/icons'
import { appContext } from 'src/lib/context'
import { parseToken } from 'src/lib/auth'
import { useAsyncRetry } from 'src/lib/async'
import { usePush } from 'src/lib/history'
import { concat } from 'src/lib/utils'
import { getUsers, importUsers } from 'src/providers/users'
import SectionView from 'src/components/views/SectionView'
import RouteNotFound from 'src/routes/RouteNotFound'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'
import InviteUsersDialog from './InviteUsersDialog'
import EditUserDialog from './EditUserDialog'

export default () => {
  const route = useRouteMatch()
  const { t, i18n } = useTranslation()
  const push = usePush()
  const { token, http, toggleProgressBar } = React.useContext(appContext)
  const [rows, setRows] = React.useState<model.User[]>([])
  const [selectedRows, setSelectedRows] = React.useState<Row[]>([])
  const tokenInfo = parseToken(token)
  const [alert, setAlert] = React.useState<boolean | null>(null)

  const { loading, error, retry } = useAsyncRetry(async () => setRows(await getUsers(http)))

  const refresh = async () => {
    if (tokenInfo?.googleAdmin) {
      await importUsers(http)
    }

    retry()
  }

  React.useEffect(() => {
    setAlert(null)
    toggleProgressBar(loading)
    return () => toggleProgressBar(false)
  }, [loading])

  return (
    <SectionView title={t('routes.users.title')}>
      <DataTable
        className="results"
        rowsPerPage={settings.rowsPerPage}
        selection
        rows={rows}
        selectedRows={selectedRows}
        onSelectedRowsChange={setSelectedRows}
        columns={[
          { name: 'user', shrink: true, title: t('routes.users.user') },
          { name: 'state', title: t('routes.users.state') }
        ]}
        headerActions={[
          {
            icon: <EmailMultipleIcon />,
            title: t('routes.users.addItem'),
            disabled: selectedRows.length == 0,
            onClick: () => push(Path.InviteUsers)
          },
          {
            icon: <RefreshIcon />,
            title: t('routes.users.refresh'),
            onClick: () => refresh()
          }
        ]}
        rowActions={row => [
          {
            icon: <EditIcon />,
            title: t('routes.users.editItem'),
            visible: tokenInfo?.admin && tokenInfo?.email !== row.email,
            onClick: row => push(Path.EditUser, { id: row.id })
          }
        ]}
        rowRenders={{
          user: row => (
            <>
              <Typography variant="body2">
                <Link href={`mailto:${row.email}`}>{row.email}</Link>
              </Typography>
              <Typography variant="body2">
                {[
                  [row.name, tokenInfo?.email == row.email && ` (${t('routes.users.me')})`]
                    .filter(Boolean)
                    .join(' '),
                  row.admin && 'Admin'
                ]
                  .filter(Boolean)
                  .join(', ')}
              </Typography>
            </>
          ),
          state: row => (
            <>
              <Typography variant="body2" color={row.emailSent ? 'textPrimary' : 'textSecondary'}>
                {row.emailSent
                  ? t('routes.users.invitationSent', {
                      date: row.emailSent.setLocale(i18n.language).toRelativeCalendar()
                    })
                  : t('routes.users.invitationNotSent')}
              </Typography>
              <Typography
                variant="body2"
                color={row.lastApiAccess ? 'textPrimary' : 'textSecondary'}
              >
                {row.lastApiAccess
                  ? t('routes.users.lastApiUsage', {
                      // TODO: set up datetime format settings
                      date: row.lastApiAccess.toLocaleString(DateTime.DATETIME_MED)
                    })
                  : t('routes.users.extensionNotInstalled')}
              </Typography>
            </>
          )
        }}
      />
      <Switch>
        <Route exact path={Path.Users} />
        <Route
          path={Path.InviteUsers}
          render={() => (
            <InviteUsersDialog
              recipients={selectedRows.map(({ id, name, email }) => ({ id, name, email }))}
              onAccept={retry}
            />
          )}
        />
        <Route path={Path.EditUser} render={() => <EditUserDialog onAccept={retry} />} />
        <Route path={concat([route.path, Path.ApiKey])} component={ApiKeyDialog} />
        <Route render={() => <RouteNotFound />} />
      </Switch>
      <Alert
        autoHide={false}
        open={alert ?? (!loading && !!error)}
        message={t(error || 'errors.unknown')}
        onClose={() => setAlert(false)}
      />
    </SectionView>
  )
}
