// Invite Users dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import { useHistory } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Button } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import * as model from '@semaforo/model'
import { SelectField } from '@semaforo/ui/fields'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { inviteUser } from 'src/providers/users'
import { getTemplates } from 'src/providers/templates'
import { DialogView } from 'src/components/views'
import TemplateEditor from 'src/components/TemplateEditor'
import clsx from 'clsx'

const useStyles = makeStyles(() => ({
  errorMsg: {
    whiteSpace: 'pre-wrap'
  }
}))

type Props = {
  recipients: {
    id: number
    name: string
    email: string
  }[]
  onAccept: () => void
}

export default ({ recipients, onAccept }: Props) => {
  const classes = useStyles()
  const history = useHistory<{ recipients?: { id: number; name: string }[] } | undefined>()
  const { t } = useTranslation()
  const { http } = React.useContext(appContext)
  const [templates, setTemplates] = React.useState<model.Template[]>([])
  const [selectedTemplate, setSelectedTemplate] = React.useState<model.Template>()
  const [text, setText] = React.useState('')

  const initState = useAsync(async () => {
    const templates = await getTemplates(http)

    setTemplates(templates)
  })

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    const message = text.trim()

    if (recipients.length == 0 || !message) {
      throw new Error(t('errors.required_fields'))
    }

    const failedEmails: { email: string; text: string }[] = []
    for (const recipient of recipients) {
      try {
        await inviteUser(http, { message, subject: 'Invitation to join Semaforo', ...recipient })
        // Wait for a moment before sending the next message,
        // so that GMail doesn't think we are spammers.
        await new Promise(r => setTimeout(r, 1000))
      } catch (err) {
        const status = _.get(err, 'response.data.status')

        if (status == 'duplicate_record') {
          failedEmails.push({
            email: recipient.email,
            text: t('routes.inviteUsers.userAlreadyExists')
          })
        } else if (status == 'invalid_email') {
          failedEmails.push({ email: recipient.email, text: t('routes.inviteUsers.invalidEmail') })
        } else {
          throw err
        }
      }
    }

    if (_.size(failedEmails) > 0) {
      const msg = failedEmails.map(({ email, text }) => `[${email}]: ${text}`).join('\n')

      throw new Error([t('routes.inviteUsers.failedToSend'), msg].join('\n\n'))
    }

    onAccept()
    history.goBack()
  }, [recipients, text])

  React.useEffect(() => {
    setText(selectedTemplate?.text || '')
  }, [selectedTemplate])

  const loading = initState.loading || submitState.loading
  const error = initState.error || submitState.error

  return (
    <DialogView
      title={t('routes.inviteUsers.title')}
      error={error && <span className={clsx([classes.errorMsg, error])}>{t(error)}</span>}
      actions={
        <>
          <Button disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton loading={loading} onClick={onSubmit}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      <SelectField
        required
        value={selectedTemplate?.id || ''}
        label={t('routes.inviteUsers.templateField')}
        options={templates.map(x => ({ value: `${x.id}`, label: x.name }))}
        onChange={templateId => setSelectedTemplate(templates.find(({ id }) => id == +templateId))}
      />
      <TemplateEditor value={text} onChange={setText} />
    </DialogView>
  )
}
