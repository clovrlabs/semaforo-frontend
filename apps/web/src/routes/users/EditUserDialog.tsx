// Edit Domain dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { TextField, SelectField } from '@semaforo/ui/fields'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { getUser, updateUser } from 'src/providers/users'
import { DialogView } from 'src/components/views'

type Props = {
  onAccept: () => void
}

export default ({ onAccept }: Props) => {
  const history = useHistory()
  const params = useParams<{ id: string }>()
  const { t } = useTranslation()
  const { http } = React.useContext(appContext)
  const [name, setName] = React.useState('')
  const [email, setEmail] = React.useState('')
  const [role, setRole] = React.useState('')
  const roles = [
    { value: 'guest', label: t('roles.guest') },
    { value: 'admin', label: t('roles.admin') }
  ]

  const initState = useAsync(async () => {
    const item = await getUser(http, { id: +params.id })

    setName(item.name)
    setEmail(item.email)
    setRole(item.admin ? 'admin' : 'guest')
  }, [params.id])

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    if (!role) {
      throw new Error('errors.required_fields')
    }

    await updateUser(http, { id: +params.id, admin: role == 'admin' })
    onAccept()
    history.goBack()
  }, [params.id, role])

  const loading = submitState.loading || initState.loading
  const error = submitState.error || initState.error

  return (
    <DialogView
      title={t('routes.editUser.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton loading={loading} onClick={onSubmit}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      <TextField label={t('routes.editUser.nameField')} value={name} disabled />
      <TextField label={t('routes.editUser.emailField')} value={email} disabled />
      <SelectField
        autoFocus
        required
        label={t('routes.editUser.roleField')}
        options={roles}
        value={role}
        onChange={setRole}
      />
    </DialogView>
  )
}
