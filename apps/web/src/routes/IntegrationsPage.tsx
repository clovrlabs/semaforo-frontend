// Integrations page.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { Switch, Route, Prompt, useRouteMatch } from 'react-router-dom'
import _ from 'lodash'
import { useValidator } from '@plastic-ui/form-validator'
import { Box } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { TextField, SwitchField } from '@semaforo/ui/fields'
import { SubmitButton } from '@semaforo/ui/buttons'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { appContext } from 'src/lib/context'
import { getCredentials, updateCredentials } from 'src/providers/integrations'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'
import { Path } from 'src/settings'
import { concat } from 'src/lib/utils'
import SectionView from 'src/components/views/SectionView'
import Alert from 'src/components/Alert'

const useStyles = makeStyles(theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    paddingBottom: theme.spacing(1)
  },
  actions: {
    display: 'flex',
    justifyContent: 'flex-end',
    paddingTop: theme.spacing(1)
  }
}))

export default () => {
  const classes = useStyles()
  const route = useRouteMatch()
  const { t } = useTranslation()
  const { http } = React.useContext(appContext)
  const [successMsg, setSuccessMsg] = React.useState('')
  const [dataSaved, setDataSaved] = React.useState(true)
  const [managedGophish, setManagedGophish] = React.useState(true)
  const [data, setData] = React.useState({
    route53AccessKey: '',
    route53AccessSecret: '',
    gophishUrl: '',
    gophishApiKey: ''
  })

  const initState = useAsync(async () => {
    const info = await getCredentials(http)

    setManagedGophish(!info.gophish_url)
    setData({
      route53AccessKey: info.route53_access_key || '',
      route53AccessSecret: info.route53_access_secret || '',
      gophishUrl: info.gophish_url || '',
      gophishApiKey: info.gophish_api_key || ''
    })
  }, [])

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    setSuccessMsg('')
    if (!validator.test(data)) {
      return
    }

    await updateCredentials(http, {
      ...data,
      gophishUrl: managedGophish ? '' : data.gophishUrl,
      gophishApiKey: managedGophish ? '' : data.gophishApiKey
    })
    setDataSaved(true)
    setSuccessMsg(t('routes.integrations.dataSaved'))
  }, [data, managedGophish])

  const onFieldChange = (key: string) => (value: string) => {
    setData({ ...data, [key]: value })
    setDataSaved(false)
  }

  // TODO: (all) use form-validator
  const validator = useValidator({
    defaultValidator: val => !!val || t<string>('errors.requiredField'),
    validators: {
      gophishUrl: [val => managedGophish || !!val || t<string>('errors.requiredField')],
      gophishApiKey: [val => managedGophish || !!val || t<string>('errors.requiredField')]
    }
  })

  const loading = submitState.loading || initState.loading
  const error = submitState.error || initState.error

  return (
    <SectionView title={t('routes.integrations.title')}>
      <Box className={classes.form}>
        <Box className={classes.content}>
          {/* TODO: (?, all) Add help button */}
          <TextField
            className="route53-field"
            autoFocus
            required
            label={t('routes.integrations.route53AccessKeyField')}
            value={data.route53AccessKey}
            onChange={onFieldChange('route53AccessKey')}
            errorText={validator.text('route')}
          />
          <TextField
            className="route53-field"
            autoFocus
            required
            label={t('routes.integrations.route53AccessSecretField')}
            value={data.route53AccessSecret}
            onChange={onFieldChange('route53AccessKey')}
            errorText={validator.text('route')}
          />
          <SwitchField
            className="managed-gophish-field"
            label={t('routes.integrations.managedGophishField')}
            value={managedGophish}
            onChange={value => {
              setManagedGophish(value)
              setDataSaved(false)
            }}
          />
          <TextField
            disabled={managedGophish}
            required={!managedGophish}
            className="gophish-url-field"
            label={t('routes.integrations.gophishUrlField')}
            value={data.gophishUrl}
            onChange={onFieldChange('gophishUrl')}
            errorText={validator.text('gophishUrl')}
          />
          <TextField
            disabled={managedGophish}
            required={!managedGophish}
            className="gophish-api-field"
            label={t('routes.integrations.gophishApiField')}
            value={data.gophishApiKey}
            onChange={onFieldChange('gophishApiKey')}
            errorText={validator.text('gophishApiKey')}
          />
        </Box>
        <Box className={classes.actions}>
          <SubmitButton className="save-btn" loading={loading} onClick={onSubmit}>
            {t('buttons.save')}
          </SubmitButton>
        </Box>
      </Box>
      <Switch>
        {/* TODO: (all) it is not practical to declare this route on each page */}
        <Route path={concat([route.path, Path.ApiKey])} component={ApiKeyDialog} />
      </Switch>
      <Prompt when={!dataSaved} message={t('routes.integrations.changesNotSaved')} />
      <Alert severity="error" open={!loading && !!error} message={t(error || '')} />
      <Alert severity="success" open={!loading && !!successMsg} message={t(successMsg)} />
    </SectionView>
  )
}
