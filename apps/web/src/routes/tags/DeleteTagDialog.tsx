// Delete Domain dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsyncFn } from 'src/lib/async'
import { deleteTag } from 'src/providers/tags'
import { DialogView } from 'src/components/views'

type Props = {
  onAccept: () => void
}

export default ({ onAccept }: Props) => {
  const history = useHistory()
  const params = useParams<{ id: string }>()
  const { t } = useTranslation()
  const { http, refreshMenu } = React.useContext(appContext)

  const { loading, error, fn: onSubmit } = useAsyncFn(async () => {
    await deleteTag(http, { id: +params.id })
    refreshMenu()
    onAccept()
    history.goBack()
  }, [params.id])

  return (
    <DialogView
      className="delete-tag-dialog"
      maxWidth="xs"
      title={t('routes.deleteTag.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button className="cancel-btn" disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton className="accept-btn" loading={loading} onClick={onSubmit}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      {t('routes.deleteTag.confirmText')}
    </DialogView>
  )
}
