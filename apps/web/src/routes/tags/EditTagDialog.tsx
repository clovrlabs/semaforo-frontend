// Edit Domain dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { Button } from '@material-ui/core'
import { TextField, SwitchField } from '@semaforo/ui/fields'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { getTag, updateTag } from 'src/providers/tags'
import { DialogView } from 'src/components/views'

type Props = {
  onAccept: () => void
}

export default ({ onAccept }: Props) => {
  const history = useHistory()
  const params = useParams<{ id: string }>()
  const { t } = useTranslation()
  const { http, refreshMenu } = React.useContext(appContext)
  const [name, setName] = React.useState('')
  const [visible, setVisible] = React.useState(false)

  const initState = useAsync(async () => {
    const item = await getTag(http, { id: +params.id })

    setName(item.name)
    setVisible(item.visible)
  }, [params.id])

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    if (!name) {
      throw new Error('errors.required_fields')
    }

    await updateTag(http, { id: +params.id, name, visible })
    refreshMenu()
    onAccept()
    history.goBack()
  }, [params.id, name, visible])

  const loading = submitState.loading || initState.loading
  const error = submitState.error || initState.error

  return (
    <DialogView
      className="edit-tag-dialog"
      title={t('routes.editTag.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button className="cancel-btn" disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton className="accept-btn" loading={loading} onClick={onSubmit}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      <TextField
        className="name-field"
        autoFocus
        required
        label={t('routes.editTag.nameField')}
        value={name}
        onChange={setName}
      />
      <SwitchField label={t('routes.editTag.visibleField')} value={visible} onChange={setVisible} />
    </DialogView>
  )
}
