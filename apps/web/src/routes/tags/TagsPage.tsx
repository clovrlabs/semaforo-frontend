// Templates page.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Switch, Route, useRouteMatch } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Switch as MuiSwitch } from '@material-ui/core'
import {
  Refresh as RefreshIcon,
  Add as AddIcon,
  Edit as EditIcon,
  Delete as DeleteIcon
} from '@material-ui/icons'
import * as model from '@semaforo/model'
import { Alert } from '@semaforo/ui'
import DataTable, { Row } from '@semaforo/ui/DataTable'
import settings, { Path } from 'src/settings'
import { appContext } from 'src/lib/context'
import { useAsyncFn, useAsyncRetry } from 'src/lib/async'
import { usePush } from 'src/lib/history'
import { concat } from 'src/lib/utils'
import { getTags, updateTag } from 'src/providers/tags'
import SectionView from 'src/components/views/SectionView'
import RouteNotFound from 'src/routes/RouteNotFound'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'
import AddTagDialog from './AddTagDialog'
import EditTagDialog from './EditTagDialog'
import DeleteTagDialog from './DeleteTagDialog'

export default () => {
  const route = useRouteMatch()
  const { t } = useTranslation()
  const push = usePush()
  const { http, refreshMenu, toggleProgressBar } = React.useContext(appContext)
  const [rows, setRows] = React.useState<model.Tag[]>([])
  const [selectedRowId, setSelectedRowId] = React.useState<React.ReactText>()
  const [alert, setAlert] = React.useState<boolean | null>(null)

  const refresh = React.useCallback(async () => {
    setRows(await getTags(http))
  }, [http])

  const { loading, error, retry } = useAsyncRetry(refresh)

  const { fn: toggleTag, ...toggleTagState } = useAsyncFn(
    async (row: Row, visible: boolean) => {
      await updateTag(http, { id: +row.id, name: row.name, visible })
      await refreshMenu()
      await refresh()
    },
    [http, loading]
  )

  React.useEffect(() => {
    setAlert(null)
    toggleProgressBar(loading || toggleTagState.loading)
    return () => toggleProgressBar(false)
  }, [loading, toggleTagState.loading])

  return (
    <SectionView title={t('routes.tags.title')}>
      <DataTable
        className="results"
        rowsPerPage={settings.rowsPerPage}
        rows={rows}
        columns={[
          { name: 'name', title: t('routes.tags.nameCol') },
          { name: 'visible', title: t('routes.tags.visibleCol'), align: 'right' }
        ]}
        headerActions={[
          {
            className: 'add-btn',
            icon: <AddIcon />,
            title: t('routes.tags.addItem'),
            onClick: () => push(Path.NewTag)
          },
          {
            className: 'refresh-btn',
            icon: <RefreshIcon />,
            title: t('routes.tags.refresh'),
            onClick: () => retry()
          }
        ]}
        rowActions={[
          {
            className: 'edit-btn',
            icon: <EditIcon />,
            title: t('routes.tags.editItem'),
            onClick: row => push(Path.EditTag, { id: row.id })
          },
          {
            className: 'delete-btn',
            icon: <DeleteIcon />,
            title: t('routes.tags.deleteItem'),
            color: 'secondary',
            onClick: row => push(Path.DeleteTag, { id: row.id })
          }
        ]}
        rowRenders={{
          visible: row => (
            <MuiSwitch
              disabled={row.id == selectedRowId}
              checked={row.visible}
              onChange={e => {
                setSelectedRowId(row.id)
                toggleTag(row, e.target.checked).finally(() => setSelectedRowId(undefined))
              }}
            />
          )
        }}
      />
      <Switch>
        <Route exact path={Path.Tags} />
        <Route path={Path.NewTag} render={() => <AddTagDialog onAccept={refresh} />} />
        <Route path={Path.EditTag} render={() => <EditTagDialog onAccept={refresh} />} />
        <Route path={Path.DeleteTag} render={() => <DeleteTagDialog onAccept={refresh} />} />
        <Route path={concat([route.path, Path.ApiKey])} component={ApiKeyDialog} />
        <Route render={() => <RouteNotFound />} />
      </Switch>
      <Alert
        autoHide={false}
        open={alert ?? (!loading && !!error)}
        message={t(error || 'errors.unknown')}
        onClose={() => setAlert(false)}
      />
    </SectionView>
  )
}
