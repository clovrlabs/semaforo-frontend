// Edit Domain dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory, useParams } from 'react-router-dom'
import { Button } from '@material-ui/core'
import * as model from '@semaforo/model'
import { TextField, DropdownSetField } from '@semaforo/ui/fields'
import { SubmitButton } from '@semaforo/ui/buttons'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { getDomain, updateDomain, getDomainTags, updateDomainTags } from 'src/providers/domains'
import { getTags } from 'src/providers/tags'
import { DialogView } from 'src/components/views'

type Props = {
  onAccept: () => void
}

export default ({ onAccept }: Props) => {
  const history = useHistory()
  const params = useParams<{ id: string }>()
  const { t } = useTranslation()
  const { http } = React.useContext(appContext)
  const [options, setOptions] = React.useState<model.Tag[]>([])
  const [tags, setTags] = React.useState<string[]>([])
  const [url, setUrl] = React.useState('')

  const initState = useAsync(async () => {
    const tags = await getTags(http)
    const domainTags = await getDomainTags(http, { id: +params.id })
    const item = await getDomain(http, { id: +params.id })

    setOptions(tags)
    setTags(domainTags.map(x => x.name))
    setUrl(item.url)
  }, [params.id])

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    if (!url) {
      throw new Error('errors.required_fields')
    }

    await updateDomain(http, { id: +params.id, url })
    await updateDomainTags(http, { id: +params.id, tags })
    onAccept()
    history.goBack()
  }, [params.id, url, tags])

  const loading = submitState.loading || initState.loading
  const error = submitState.error || initState.error

  return (
    <DialogView
      className="edit-domain-dialog"
      title={t('routes.editDomain.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button className="cancel-btn" disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          <SubmitButton className="accept-btn" loading={loading} onClick={onSubmit}>
            {t('buttons.continue')}
          </SubmitButton>
        </>
      }
    >
      <TextField
        className="url-field"
        autoFocus
        required
        label={t('routes.editDomain.urlField')}
        value={url}
        onChange={setUrl}
      />
      <DropdownSetField
        className="tags-field"
        label={t('routes.editDomain.tagsField')}
        value={tags}
        onChange={value => setTags(value as string[])}
        options={options.map(x => x.name)}
        placeholder={t('routes.editDomain.tagsPlaceholder')}
      />
    </DialogView>
  )
}
