// Add Domain dialog.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { useTranslation } from 'react-i18next'
import { useHistory } from 'react-router-dom'
// TODO: move Button to @semaforo/ui/buttons
import { Button } from '@material-ui/core'
import { DataTable, Row } from '@semaforo/ui'
import { SubmitButton } from '@semaforo/ui/buttons'
import settings from 'src/settings'
import { appContext } from 'src/lib/context'
import { useAsync, useAsyncFn } from 'src/lib/async'
import { PromiseType } from 'src/lib/types'
import { getAvailableDomains, createDomain } from 'src/providers/domains'
import { DialogView } from 'src/components/views'

type Props = {
  onAccept: () => void
}

export default ({ onAccept }: Props) => {
  const { t } = useTranslation()
  const history = useHistory()
  const { http } = React.useContext(appContext)
  const [rows, setRows] = React.useState<PromiseType<typeof getAvailableDomains>>([])
  const [selectedRows, setSelectedRows] = React.useState<Row[]>([])

  const initState = useAsync(async () => {
    // TODO: do not show already added domains
    setRows(await getAvailableDomains(http))
  }, [])

  const { fn: onSubmit, ...submitState } = useAsyncFn(async () => {
    // TODO: what happens if a request fails?
    await Promise.all(selectedRows.map(row => createDomain(http, { url: row.url })))
    onAccept()
    history.goBack()
  }, [selectedRows])

  const loading = initState.loading || submitState.loading
  const error = initState.error || submitState.error

  return (
    <DialogView
      className="import-domains-dialog"
      title={t('routes.importDomains.title')}
      error={error && <span className={error}>{t(error)}</span>}
      actions={
        <>
          <Button className="cancel-btn" disabled={loading} onClick={() => history.goBack()}>
            {t('buttons.cancel')}
          </Button>
          {/* TODO: disable button when no items are selected */}
          <SubmitButton
            className="accept-btn"
            loading={loading}
            disabled={selectedRows.length == 0}
            onClick={onSubmit}
          >
            {t('routes.importDomains.importDomainsBtn')}
          </SubmitButton>
        </>
      }
    >
      {/* TODO: create a custom DataTable */}
      <DataTable
        className="results"
        selection
        selectedRows={selectedRows}
        onSelectedRowsChange={setSelectedRows}
        rowsPerPage={settings.rowsPerPage}
        rows={rows}
        columns={[{ name: 'url', title: t('routes.importDomains.urlCol') }]}
      />
    </DialogView>
  )
}
