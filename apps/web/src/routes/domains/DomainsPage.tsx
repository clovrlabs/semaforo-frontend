// Domains page.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Switch, Route, Link, useLocation, useRouteMatch, useParams } from 'react-router-dom'
import { useTranslation } from 'react-i18next'
import { Typography, Breadcrumbs } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {
  Refresh as RefreshIcon,
  Add as AddIcon,
  Edit as EditIcon,
  Delete as DeleteIcon
} from '@material-ui/icons'
import * as model from '@semaforo/model'
import { DataTable, Alert } from '@semaforo/ui'
import settings, { Path } from 'src/settings'
import { getDomains } from 'src/providers/domains'
import { getTagDomains } from 'src/providers/tags'
import { isAdmin } from 'src/lib/auth'
import { usePush, replace } from 'src/lib/history'
import { appContext } from 'src/lib/context'
import { useAsyncRetry } from 'src/lib/async'
import { concat } from 'src/lib/utils'
import { ImportIcon } from 'src/components/icons'
import SectionView from 'src/components/views/SectionView'
import RouteNotFound from 'src/routes/RouteNotFound'
import ImportDomainsDialog from './ImportDomainsDialog'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'
import EditDomainDialog from './EditDomainDialog'
import AddDomainDialog from './AddDomainDialog'
import DeleteDomainDialog from './DeleteDomainDialog'

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: theme.spacing(3)
  },
  tagItem: {
    marginRight: theme.spacing(2)
  }
}))

export default () => {
  const route = useRouteMatch()
  const { t } = useTranslation()
  const classes = useStyles()
  const push = usePush()
  const location = useLocation()
  const params = useParams<{ tagId: string }>()
  const { http, tags, token, toggleProgressBar } = React.useContext(appContext)
  const [rows, setRows] = React.useState<model.Domain[]>([])
  const [alert, setAlert] = React.useState<boolean | null>(null)
  const admin = isAdmin(token)

  const breadcrumbs = [
    { path: '/tags/0/domains', name: t('menu.domains') },
    ...tags
      .filter(({ id }) => params.tagId == `${id}`)
      .map(({ id, name }) => ({ path: `/tags/${id}/domains`, name }))
  ]

  const { loading, error, retry } = useAsyncRetry(async () => {
    const { tagId } = params
    const items = await (+tagId > 0 ? getTagDomains(http, { tagId }) : getDomains(http))

    setRows(items)
  }, [params.tagId])

  const refresh = () => !loading && retry()

  React.useEffect(() => {
    setAlert(null)
    toggleProgressBar(loading)
    return () => toggleProgressBar(false)
  }, [loading])

  return (
    <SectionView
      title={
        <Breadcrumbs separator="›" className={classes.title}>
          {breadcrumbs.map(item => (
            <Typography key={item.path} variant="h4" color="textPrimary">
              {location.pathname.startsWith(item.path) ? (
                item.name
              ) : (
                <Link key={item.path} to={item.path}>
                  {item.name}
                </Link>
              )}
            </Typography>
          ))}
        </Breadcrumbs>
      }
    >
      <DataTable
        className="results"
        rowsPerPage={settings.rowsPerPage}
        rows={rows}
        columns={[
          { name: 'url', title: t('routes.domains.urlCol') },
          { name: 'tags', title: t('routes.domains.tagsCol') }
        ]}
        headerActions={[
          ...(admin
            ? [
                {
                  className: 'import-btn',
                  icon: <ImportIcon />,
                  title: t('routes.domains.importDomains'),
                  onClick: () => push(Path.ImportDomains, { tagId: params.tagId })
                },
                {
                  className: 'add-btn',
                  icon: <AddIcon />,
                  title: t('routes.domains.addItem'),
                  onClick: () => push(Path.NewDomain, { tagId: params.tagId })
                }
              ]
            : []),
          // TODO: (?) do not replace the refresh button. Instead add a new import button
          {
            className: 'refresh-btn',
            icon: <RefreshIcon />,
            title: t('routes.domains.refresh'),
            onClick: () => retry()
          }
        ]}
        rowActions={
          admin
            ? [
                {
                  className: 'edit-btn',
                  icon: <EditIcon />,
                  title: t('routes.domains.editItem'),
                  onClick: row => push(Path.EditDomain, { tagId: params.tagId, id: row.id })
                },
                {
                  className: 'delete-btn',
                  icon: <DeleteIcon />,
                  title: t('routes.domains.deleteItem'),
                  color: 'secondary',
                  onClick: row => push(Path.DeleteDomain, { tagId: params.tagId, id: row.id })
                }
              ]
            : []
        }
        rowRenders={{
          tags: ({ tags }) =>
            tags.length > 0 ? (
              <>
                {tags.map((tag: model.Tag) => (
                  <Link
                    key={tag.id}
                    className={classes.tagItem}
                    to={tag.visible ? replace(Path.Domains, { tagId: tag.id }) : '/tags'}
                  >
                    {tag.name}
                  </Link>
                ))}
              </>
            ) : (
              '–'
            )
        }}
      />
      <Switch>
        <Route exact path={Path.Domains} />
        <Route
          path={Path.ImportDomains}
          render={() => <ImportDomainsDialog onAccept={refresh} />}
        />
        <Route path={Path.NewDomain} render={() => <AddDomainDialog onAccept={refresh} />} />
        <Route path={Path.EditDomain} render={() => <EditDomainDialog onAccept={refresh} />} />
        <Route path={Path.DeleteDomain} render={() => <DeleteDomainDialog onAccept={refresh} />} />
        <Route path={concat([route.path, Path.ApiKey])} component={ApiKeyDialog} />
        <Route render={() => <RouteNotFound />} />
      </Switch>
      <Alert
        autoHide={false}
        open={alert ?? (!loading && !!error)}
        message={t(error || 'errors.unknown')}
        onClose={() => setAlert(false)}
      />
    </SectionView>
  )
}
