// User providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'
import { DateTime } from 'luxon'
import _ from 'lodash'
import * as model from '@semaforo/model'

export const getUsers = async (http: AxiosInstance): Promise<model.User[]> => {
  const res = await http.get<{
    items: {
      id: number
      email: string
      admin: boolean
      name: string
      email_sent?: string
      last_api_access?: string
    }[]
  }>('/users')

  return res.data.items.map(({ email_sent, last_api_access, ...rest }) => ({
    emailSent: email_sent ? DateTime.fromISO(email_sent) : undefined,
    lastApiAccess: last_api_access ? DateTime.fromISO(last_api_access) : undefined,
    ...rest
  }))
}

export const getUser = async (http: AxiosInstance, params: { id: number }): Promise<model.User> => {
  const { id } = params
  const res = await http.get(`/users/${id}`)

  return res.data.item
}

export const updateUser = async (
  http: AxiosInstance,
  params: {
    id: number
    admin: boolean
  }
): Promise<void> => {
  const { id, admin } = params
  console.log(admin)
  if (admin) {
    await http.post(`/users/make_admin/${id}`)
  } else {
    await http.delete(`/users/make_admin/${id}`)
  }
}

export const inviteUser = async (
  http: AxiosInstance,
  params: {
    id: number
    name: string
    subject: string
    message: string
  }
): Promise<{
  id: number
}> => {
  // escape template vars
  const { protocol, host } = window.location
  const appUrl = _.trimEnd([protocol, host].join('//'), '/')
  const { id, name, subject, message } = params
  const text = message
    .replace(/\{\{\s*url\s*}\}/g, appUrl)
    .replace(/\{\{\s*api-key-url\s*}\}/g, `${appUrl}/api-key`)
    .replace(/\{\{\s*username\s*}\}/g, name)
  const res = await http.post(`/users/${id}/invite`, { subject, message: text })

  return res.data
}

export const deleteUser = async (http: AxiosInstance, id: number): Promise<void> => {
  await http.delete(`/users/${id}`)
}

export const importUsers = async (http: AxiosInstance): Promise<void> => {
  await http.post('/users/import')
}
