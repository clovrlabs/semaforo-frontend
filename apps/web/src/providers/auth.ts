// Authentication providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'

/**
 * Login in the system and returns a JSON Web Token (JWT).
 *
 * @param idToken     Google ID token
 * @param accessToken Google access token
 */
export const login = async (
  http: AxiosInstance,
  idToken: string,
  accessToken: string
): Promise<{
  token: string
}> => {
  const res = await http.post('/login', { id_token: idToken, access_token: accessToken })

  return res.data
}
