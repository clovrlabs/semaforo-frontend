// Tag providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'
import * as model from '@semaforo/model'
import { buildUrl } from 'src/lib/http'

export const getTags = async (
  http: AxiosInstance,
  params?: { visible?: boolean }
): Promise<model.Tag[]> => {
  const res = await http.get<{
    items: {
      id: number
      name: string
      visible: number
    }[]
  }>(buildUrl('/tags', params))
  const { items } = res.data

  return items.map(({ visible, ...rest }) => ({ ...rest, visible: visible > 0 }))
}

export const getTagDomains = async (
  http: AxiosInstance,
  params: { tagId: string }
): Promise<model.Domain[]> => {
  const { tagId } = params
  const res = await http.get(`/tags/${tagId}/domains`)

  return res.data.items
}

export const getTag = async (http: AxiosInstance, params: { id: number }): Promise<model.Tag> => {
  const { id } = params
  const res = await http.get(`/tags/${id}`)
  const { item } = res.data

  return { id: item.id, name: item.name, visible: item.visible > 0 }
}

export const createTag = async (
  http: AxiosInstance,
  params: {
    name: string
    visible: boolean
  }
): Promise<number> => {
  const { name, visible } = params
  const res = await http.post('/tags', { name, visible: +visible })

  return res.data.id
}

export const updateTag = async (
  http: AxiosInstance,
  params: {
    id: number
    name: string
    visible: boolean
  }
): Promise<void> => {
  const { id, name, visible } = params
  await http.patch(`/tags/${id}`, { name, visible: +visible })
}

export const deleteTag = async (
  http: AxiosInstance,
  params: {
    id: number
  }
): Promise<void> => {
  const { id } = params
  await http.delete(`/tags/${id}`)
}
