// Domain providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'
import _ from 'lodash'
import * as model from '@semaforo/model'
import { equal } from 'src/lib/utils'
import { getTags, createTag } from './tags'

export const getDomains = async (http: AxiosInstance): Promise<model.Domain[]> => {
  const res = await http.get('/domains')

  return res.data.items
}

export const getDomain = async (
  http: AxiosInstance,
  params: { id: number }
): Promise<model.Domain> => {
  const { id } = params
  const res = await http.get(`/domains/${id}`)

  return res.data.item
}

export const updateDomain = async (
  http: AxiosInstance,
  params: {
    id: number
    url: string
  }
): Promise<void> => {
  const { id, url } = params
  await http.patch(`/domains/${id}`, { url })
}

export const createDomain = async (
  http: AxiosInstance,
  params: { url: string }
): Promise<number> => {
  const { url } = params
  const res = await http.post('/domains', { url })

  return res.data.id
}

export const deleteDomain = async (
  http: AxiosInstance,
  params: {
    id: number
  }
): Promise<void> => {
  const { id } = params
  await http.delete(`/domains/${id}`)
}

export const getDomainTags = async (
  http: AxiosInstance,
  params: {
    id: number
  }
): Promise<model.Tag[]> => {
  const { id } = params
  const res = await http.get(`/domains/${id}/tags`)

  return res.data.items
}

export const updateDomainTags = async (
  http: AxiosInstance,
  params: {
    id: number
    tags: string[]
  }
): Promise<void> => {
  const { id, tags } = params
  const options = await getTags(http)

  // creates new tags
  const newTags = tags.filter(x => !options.some(y => equal(x, y.name)))
  const newTagIds = await Promise.all(
    newTags.map(name => createTag(http, { name, visible: false }))
  )

  // gets existing tags
  const tagIds = options.filter(x => tags.some(y => equal(y, x.name))).map(x => x.id)

  await http.patch(`/domains/${id}/tags`, { tag_ids: [...tagIds, ...newTagIds] })
}

// TODO: (back-end) implement end-point
export const getAvailableDomains = async (
  http: AxiosInstance
): Promise<{ id: string; url: string }[]> => {
  const res = await http.get<{
    zones: {
      id: string
      name: string
      private: boolean
    }[]
  }>(`/import/zones`)

  return res.data.zones.map(item => ({ id: item.id, url: _.trimEnd(item.name, '.') }))
}
