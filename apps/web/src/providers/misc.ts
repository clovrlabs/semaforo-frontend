// Mixed providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'

export const getApiKey = async (http: AxiosInstance): Promise<string> => {
  const res = await http.get<{
    api_key: string
  }>('/api-key')

  return res.data.api_key
}

export const newApiKey = async (http: AxiosInstance): Promise<string> => {
  const res = await http.post<{
    api_key: string
  }>('/api-key')

  return res.data.api_key
}

export const removeAccount = async (http: AxiosInstance): Promise<void> => {
  await http.delete('/remove-account')
}
