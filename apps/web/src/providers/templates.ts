// Template providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'
import * as model from '@semaforo/model'

export const getTemplates = async (http: AxiosInstance): Promise<model.Template[]> => {
  const res = await http.get('/templates')

  return res.data.items
}

export const getTemplate = async (
  http: AxiosInstance,
  params: { id: number }
): Promise<model.Template> => {
  const { id } = params
  const res = await http.get(`/templates/${id}`)

  return res.data.item
}

export const createTemplate = async (
  http: AxiosInstance,
  params: {
    name: string
    text: string
  }
): Promise<{
  id: number
}> => {
  const { name, text } = params
  const res = await http.post('/templates', { name, text })

  return res.data.id
}

export const updateTemplate = async (
  http: AxiosInstance,
  params: {
    id: number
    name: string
    text: string
  }
): Promise<void> => {
  const { id, name, text } = params
  await http.patch(`/templates/${id}`, { name, text })
}

export const deleteTemplate = async (
  http: AxiosInstance,
  params: { id: number }
): Promise<void> => {
  const { id } = params
  await http.delete(`/templates/${id}`)
}
