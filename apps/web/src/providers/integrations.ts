// Tag providers.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { AxiosInstance } from 'axios'

export const getCredentials = async (
  http: AxiosInstance
): Promise<{
  route53_access_key: string | null
  route53_access_secret: string | null
  gophish_api_key: string | null
  gophish_url: string | null
}> => {
  const res = await http.get('/organization/credentials')

  return res.data.credentials
}

export const updateCredentials = async (
  http: AxiosInstance,
  params: {
    route53AccessKey: string
    gophishApiKey: string
    gophishUrl: string
  }
): Promise<void> => {
  const { route53AccessKey, gophishApiKey, gophishUrl } = params

  await http.post('/organization/credentials', {
    credentials: {
      route53_access_key: route53AccessKey,
      gophish_api_key: gophishApiKey,
      gophish_url: gophishUrl
    }
  })
}
