import React from 'react'
import ReactDOM from 'react-dom'
import App from './App'
import './index.css'

const renderApp = () => {
  ReactDOM.render(<App />, document.getElementById('root'))
}

// wait until `window.gapi` has been loaded
let tries = 0
const interval = setInterval(() => {
  // waits for 10 seconds. If not, shows an error.
  tries++
  if (tries > 100) {
    clearInterval(interval)
    const root = document.getElementById('root')
    if (root) {
      root.innerText = 'Service Unavailable.'
    }
  }

  if (window.gapi) {
    clearInterval(interval)
    renderApp()
  }
}, 100)
