// async / await related functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import _ from 'lodash'
import { DependencyList } from 'react'
import {
  useAsync as _useAsync,
  useAsyncFn as _useAsyncFn,
  useAsyncRetry as _useAsyncRetry
} from 'react-use'
import { FnReturningPromise, PromiseType } from 'react-use/lib/util'

const parseError = (err: Error): string => {
  const response = _.get(err, 'response')

  if (response) {
    const dataStatus = _.get(response, 'data.status')
    if (dataStatus) {
      return `errors.${dataStatus}`
    }

    const status = _.get(response, 'status')
    return status == 404 ? `errors.not_found` : 'errors.network_error'
  }

  return err.message
}

export const useAsync = <T extends FnReturningPromise>(
  fn: T,
  deps?: DependencyList
): {
  value?: PromiseType<ReturnType<T>>
  loading: boolean
  error?: string
} => {
  const { error, ...rest } = _useAsync<T>(fn, deps)

  return { ...rest, ...(error && { error: parseError(error) }) }
}

export const useAsyncFn = <T extends FnReturningPromise>(
  fn: T,
  deps?: DependencyList
): {
  value?: PromiseType<ReturnType<T>>
  loading: boolean
  error?: string
  fn: T
} => {
  const [{ error, ...rest }, callback] = _useAsyncFn<T>(fn, deps)

  return { ...rest, fn: callback, ...(error && { error: parseError(error) }) }
}

export const useAsyncRetry = <T>(
  fn: () => Promise<T>,
  deps?: DependencyList
): {
  value?: T
  loading: boolean
  error?: string
  retry: () => void
} => {
  const { error, ...rest } = _useAsyncRetry<T>(fn, deps)

  return { ...rest, ...(error && { error: parseError(error) }) }
}
