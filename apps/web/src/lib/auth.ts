// Authentication functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import jwtDecode from 'jwt-decode'

export type Token = {
  username: string
  email: string
  admin: boolean
  googleAdmin: boolean
}

export const parseToken = (token: string): Token | null => {
  try {
    const t = jwtDecode<{
      username: string
      email: string
      admin: boolean
      google_admin: boolean
    }>(token)

    return {
      username: t.username,
      email: t.email,
      admin: t.admin,
      googleAdmin: t.google_admin
    }
  } catch {
    return null
  }
}

export const isAdmin = (token: string) => {
  const info = parseToken(token)

  return info === null ? false : info.admin
}
