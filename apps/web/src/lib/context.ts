// Application contexts.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import axios, { AxiosInstance } from 'axios'
import React from 'react'
import * as model from '@semaforo/model'
import { Config } from './types'

export const appContext = React.createContext<{
  config: Config
  token: string
  tags: model.Tag[]
  http: AxiosInstance
  login: () => Promise<{ token: string }>
  logout: () => void
  toggleMenu: () => void
  toggleRemoveAccountDialog: () => void
  toggleProgressBar: (show?: boolean) => void
  refreshMenu: () => Promise<void>
}>({
  config: {
    apiUrl: '',
    supportEmail: '',
    extensions: {
      chrome: '',
      firefox: ''
    },
    links: {
      quickstarts: '',
      communityAnnouncements: ''
    },
    google: {
      clientId: ''
    }
  },
  token: '',
  tags: [],
  http: axios.create(),
  login: () => Promise.resolve({ token: '' }),
  logout: () => {},
  toggleMenu: () => {},
  toggleRemoveAccountDialog: () => {},
  toggleProgressBar: () => {},
  refreshMenu: () => Promise.resolve()
})
