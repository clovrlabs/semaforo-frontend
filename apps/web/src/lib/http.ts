// HTTP functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import _ from 'lodash'

export const buildParams = (params: { [key: string]: any }) => {
  return _.map(params, (val, key) => {
    let v = val
    if (typeof val == 'boolean') {
      v = +val
    } else if (Array.isArray(val)) {
      v = val.join(',')
    }

    return v === undefined || v === null
      ? null
      : `${encodeURIComponent(key)}=${encodeURIComponent(v)}`
  })
    .filter(x => x !== null)
    .join('&')
}

export const buildUrl = (url: string, params?: { [key: string]: any }) => {
  const query = params ? buildParams(params) : ''
  return [url, query].filter(x => !!x).join('?')
}
