// Browser's history related functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import { useHistory } from 'react-router-dom'
import * as regex from './regex'

export const replace = (path: string, params: { [key: string]: number | string }) => {
  let ret = path
  for (const key in params) {
    const re = new RegExp(`:${regex.escape(key)}`, 'g')
    ret = ret.replace(re, `${params[key]}`)
  }

  if (ret.search(/:\w+/) > -1) {
    console.error(`Missing param in: ${path}`)
  }

  return ret
}

export const usePush = () => {
  const history = useHistory()

  return (path: string, params?: { [key: string]: number | string }) => {
    history.push(params ? replace(path, params) : path)
  }
}
