// Internationalization functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

/**
 * Gets the 'short' part of a "locale code".
 *
 * For example:
 *    getShortLang('en-US') // returns 'en'
 *    getShortLang('es')    // returns 'es'
 */
export const getShortLang = (lang: string) => {
  const matches = lang.match(/^\w+/)

  return matches ? matches[0] : ''
}
