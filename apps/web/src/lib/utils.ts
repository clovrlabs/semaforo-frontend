// General purpose functions.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.
import _ from 'lodash'

export const equal = (
  v1: React.ReactText,
  v2: React.ReactText,
  options: { caseSensitive: boolean } = { caseSensitive: false }
) => {
  const { caseSensitive } = options
  const [a, b] = [`${v1}`, `${v2}`]
  return caseSensitive ? a == b : a.toLocaleLowerCase() == b.toLocaleLowerCase()
}

export const concat = (paths: string[], glue = '/') => {
  const len = paths.length

  return paths
    .map((x, i) => {
      let path = x
      if (i > 0) {
        path = _.trimStart(path, glue)
      }
      if (i < len - 1) {
        path = _.trimEnd(path, glue)
      }

      return path
    })
    .join(glue)
}
