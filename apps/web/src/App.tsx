// Main application entry point
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import TOML from 'toml'
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom'
import { LastLocationProvider } from 'react-router-last-location'
import { useTranslation } from 'react-i18next'
import { ThemeProvider } from '@material-ui/styles'
import {
  CssBaseline,
  Container,
  Divider,
  Toolbar,
  Hidden,
  Drawer,
  IconButton,
  Box,
  Link
} from '@material-ui/core'
import { ChevronLeft as ChevronLeftIcon } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import * as model from '@semaforo/model'
import axios from 'axios'
import _ from 'lodash'
import './i18n'
import settings, { Path, theme } from 'src/settings'
import { Config } from 'src/lib/types'
import { appContext } from 'src/lib/context'
import { useAsync } from 'src/lib/async'
import { login as loginProvider } from 'src/providers/auth'
import { getTags } from 'src/providers/tags'
import Header from 'src/components/Header'
import Menu from 'src/components/Menu'
import { RemoveAccountDialog } from 'src/components/dialogs'
import { AppLoading, AppError } from 'src/components/notifications'
import ProtectedRoute from 'src/components/ProtectedRoute'
import RouteNotFound from 'src/routes/RouteNotFound'
import LoginDialog from 'src/routes/LoginDialog'
import DomainsPage from 'src/routes/domains/DomainsPage'
import TagsPage from 'src/routes/tags/TagsPage'
import UsersPage from 'src/routes/users/UsersPage'
import IntegrationsPage from 'src/routes/IntegrationsPage'
import TemplatesPage from 'src/routes/templates/TemplatesPage'
import ApiKeyDialog from 'src/routes/ApiKeyDialog'

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative'
  },
  wrapper: {
    display: 'flex',
    flexDirection: 'column',
    height: '100vh'
  },
  wrapperTop: {
    display: 'flex',
    flexGrow: 1
  },
  wrapperTopLeft: {
    backgroundColor: theme.palette.background.paper,
    borderRight: `1px solid ${theme.palette.divider}`,
    minWidth: theme.spacing(settings.menuWidth)
  },
  wrapperTopRight: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2)
  },
  wrapperBottom: {
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.main,
    textAlign: 'center',
    padding: `${theme.spacing(1.5)}px 0`
  },
  drawerHeader: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    ...theme.mixins.toolbar
  },
  drawerPaper: {
    minWidth: theme.spacing(settings.menuWidth)
  }
}))

type AppProps = {
  auth: gapi.auth2.GoogleAuth
  config: any
}

const App = ({ auth, config }: AppProps) => {
  const classes = useStyles()
  const [token, setToken] = React.useState(sessionStorage.getItem('token') || '')
  const [removeAccountDialogOpen, setRemoveAccountDialogOpen] = React.useState(false)
  const [menuOpen, setMenuOpen] = React.useState(false)
  const [showProgressBar, setShowProgressBar] = React.useState(false)
  const [tags, setTags] = React.useState<model.Tag[]>([])

  const login = async () => {
    const user = await auth.signIn()
    const { id_token, access_token } = user.getAuthResponse()

    // eslint-disable-next-line
    const data = await loginProvider(http, id_token, access_token)
    const { token } = data

    sessionStorage.setItem('token', token)
    setToken(token)

    return data
  }

  const logout = () => {
    sessionStorage.removeItem('token')
    setToken('')
    auth.signOut()
  }

  const toggleMenu = () => {
    setMenuOpen(!menuOpen)
  }

  const toggleRemoveAccountDialog = () => {
    setRemoveAccountDialogOpen(!removeAccountDialogOpen)
  }

  const toggleProgressBar = (value?: boolean) => {
    setShowProgressBar(show => value ?? !show)
  }

  const http = React.useMemo(() => {
    const ret = axios.create({
      baseURL: config.apiUrl,
      headers: {
        ...(token && { Authorization: `Bearer ${token}` })
      }
    })

    ret.interceptors.response.use(
      res => res,
      err => {
        const status = err?.response?.status
        if (status == 401) {
          logout()
        }
        return Promise.reject(err)
      }
    )

    return ret
  }, [token])

  const refreshMenu = async () => {
    const tags = await getTags(http, { visible: true })

    setTags(_.sortBy(tags, 'name'))
  }

  React.useEffect(() => {
    token && refreshMenu()
  }, [token])

  return (
    <appContext.Provider
      value={{
        config,
        token,
        tags,
        http,
        login,
        logout,
        refreshMenu,
        toggleMenu,
        toggleRemoveAccountDialog,
        toggleProgressBar
      }}
    >
      <RemoveAccountDialog open={removeAccountDialogOpen} />
      <Container className={classes.container} disableGutters maxWidth="lg">
        <Header showProgressBar={showProgressBar} position="absolute" />
        <Box className={classes.wrapper}>
          <Box className={classes.wrapperTop}>
            {token && (
              <>
                {/* Main menu (mobile version) */}
                <Hidden mdUp>
                  <Drawer
                    open={menuOpen}
                    onClose={toggleMenu}
                    ModalProps={{ keepMounted: true }}
                    classes={{ paper: classes.drawerPaper }}
                  >
                    <Box className={classes.drawerHeader}>
                      <IconButton onClick={toggleMenu}>
                        <ChevronLeftIcon />
                      </IconButton>
                    </Box>
                    <Divider />
                    <Menu />
                  </Drawer>
                </Hidden>
                {/* Main menu (desktop version) */}
                <Hidden smDown>
                  <Box className={classes.wrapperTopLeft}>
                    <Toolbar />
                    <Menu />
                  </Box>
                </Hidden>
              </>
            )}
            <Box className={classes.wrapperTopRight}>
              <Toolbar />
              <Switch>
                <ProtectedRoute token={token} path={Path.ApiKey} component={ApiKeyDialog} />
                <ProtectedRoute token={token} path={Path.Domains} component={DomainsPage} />
                <ProtectedRoute token={token} adminAccess path={Path.Tags} component={TagsPage} />
                <ProtectedRoute token={token} adminAccess path={Path.Users} component={UsersPage} />
                <ProtectedRoute
                  token={token}
                  adminAccess
                  path={Path.Integrations}
                  component={IntegrationsPage}
                />
                <ProtectedRoute
                  token={token}
                  adminAccess
                  path={Path.Templates}
                  component={TemplatesPage}
                />
                <Route
                  exact
                  path={Path.Home}
                  render={props => {
                    const { state } = props.location
                    const page = _.get(state, 'referrer') || '/tags/0/domains'

                    return token ? <Redirect to={page} /> : <LoginDialog />
                  }}
                />
                <ProtectedRoute token={token} component={RouteNotFound} />
              </Switch>
            </Box>
          </Box>
          <Box className={classes.wrapperBottom}>
            <Link href={`mailto:${config.supportEmail}`} color="inherit" variant="body1">
              {config.supportEmail}
            </Link>
          </Box>
        </Box>
      </Container>
    </appContext.Provider>
  )
}

const AppContainer = (props: AppProps) => {
  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <BrowserRouter>
        <LastLocationProvider>
          <App {...props} />
        </LastLocationProvider>
      </BrowserRouter>
    </ThemeProvider>
  )
}

const AuthLoader = ({ config }: { config: Config }) => {
  const { t } = useTranslation()
  const [auth, setAuth] = React.useState<gapi.auth2.GoogleAuth | undefined>()
  const [error, setError] = React.useState('')

  React.useEffect(() => {
    window.gapi.load('auth2', () =>
      gapi.auth2
        .init({
          client_id: config.google.clientId,
          scope: settings.google.scopes.join(' ')
        })
        .then(setAuth, ({ details }) => {
          console.error(details)
          setError(details)
        })
    )
  }, [])

  return auth ? (
    <AppContainer auth={auth} config={config} />
  ) : error ? (
    <AppError message={t('errors.unknown')} />
  ) : (
    <AppLoading message={t('messages.loadingGoogleAPI')} />
  )
}

const ConfigLoader = () => {
  const { t } = useTranslation()
  const [conf, setConf] = React.useState<Config>()

  const { error } = useAsync(async () => {
    const res = await axios.get(settings.configPath)

    let conf: any
    try {
      conf = TOML.parse(res.data)
    } catch {
      throw new Error(t('errors.invalidConfig'))
    }

    const requiredFields = [
      'api_url',
      'support_email',
      'google.client_id',
      'extensions.chrome',
      'extensions.firefox',
      'links.quickstarts',
      'links.community_announcements'
    ]
    const missingFields = requiredFields.filter(x => !_.get(conf, x))
    if (missingFields.length > 0) {
      throw new Error(t('errors.requiredConfigFields', { names: missingFields.join('\n') }))
    }

    setConf({
      apiUrl: conf.api_url,
      supportEmail: conf.support_email,
      extensions: {
        chrome: conf.extensions.chrome,
        firefox: conf.extensions.firefox
      },
      links: {
        quickstarts: conf.links?.quickstarts,
        communityAnnouncements: conf.links?.community_announcements
      },
      google: {
        clientId: conf.google.client_id
      }
    })
  })

  return conf ? (
    <AuthLoader config={conf} />
  ) : error ? (
    <AppError message={error == 'errors.not_found' ? t('errors.configNotFound') : error} />
  ) : (
    <AppLoading message={t('messages.loadingConfig')} />
  )
}

export default ConfigLoader
