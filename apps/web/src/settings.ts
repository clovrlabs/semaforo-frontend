// Application config.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import _ from 'lodash'
import { createMuiTheme } from '@material-ui/core/styles'

const settings = {
  configPath: '/config.toml',
  defaultLanguage: 'en',
  menuWidth: 32,
  rowsPerPage: 10,
  theme: {
    primaryColor: '#455a64',
    secondaryColor: '#f4511e'
  },
  textEditor: {
    buttons: [['undo', 'redo'], ['bold', 'italic', 'link'], ['image']],
    imageUploadSizeLimit: 0,
    imageUploadUrl: ''
  },
  google: {
    scopes: ['email', 'profile', 'https://www.googleapis.com/auth/admin.directory.user.readonly']
  }
}

/**
 * Routes.
 */
export enum Path {
  ApiKey = '/api-key',
  // TODO: (?) remove this route
  RemoveAccount = '/remove-account',
  // home routes
  Home = '/',
  // domain routes
  Domains = '/tags/:tagId/domains',
  NewDomain = '/tags/:tagId/domains/new',
  ImportDomains = '/tags/:tagId/domains/import',
  EditDomain = '/tags/:tagId/domains/:id/edit',
  DeleteDomain = '/tags/:tagId/domains/:id/delete',
  // tag routes
  Tags = '/tags',
  NewTag = '/tags/new',
  EditTag = '/tags/:id/edit',
  DeleteTag = '/tags/:id/delete',
  // user routes
  Users = '/users',
  InviteUsers = '/users/invite',
  EditUser = '/users/:id/edit',
  // template routes
  Integrations = '/integrations',
  Templates = '/templates',
  NewTemplate = '/templates/new',
  EditTemplate = '/templates/:id/edit',
  DeleteTemplate = '/templates/:id/delete'
}

/**
 * Theme.
 *
 * Palette generator:
 * https://material.io/resources/color/#!/?view.left=0&view.right=0&primary.color=455A64&secondary.color=F4511E
 */
export const theme = createMuiTheme({
  typography: {
    // h1 ... h6
    ..._.mapValues(_.keyBy(_.range(6).map(i => `h${i + 1}`)), h => ({
      fontFamily: ['Quicksand', 'sans-serif'].join(', '),
      ...(h == 'h4' && { fontSize: '1.8rem' })
    })),
    fontFamily: ['Raleway', 'sans-serif'].join(', '),
    fontWeightRegular: 500,
    fontWeightMedium: 600
  },
  palette: {
    primary: {
      main: settings.theme.primaryColor
    },
    secondary: {
      main: settings.theme.secondaryColor
    }
  }
})

export default settings
