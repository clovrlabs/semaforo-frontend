// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
// import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

// before(() => {
//   const apiUrl = Cypress.env('API_URL')
//   cy.request('POST', `${apiUrl}/token`, { email: 'owner@gonzaloch.com' })
//     .its('body')
//     .then(body => sessionStorage.setItem('token', body.token))
// })

Cypress.Commands.add('resetDb', () => {
  return cy.exec('yarn workspace @semaforo/server db:reset')
})

Cypress.Commands.add('login', () => {
  const apiUrl = Cypress.env('API_URL')

  return cy
    .request('POST', `${apiUrl}/token`, { email: 'owner@gonzaloch.com' })
    .its('body')
    .then(body => sessionStorage.setItem('token', body.token))
})
