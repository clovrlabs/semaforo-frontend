/// <reference path="../support/index.d.ts" />

context('Create, edit and delete tags', () => {
  before(cy.login)

  beforeEach(() => {
    cy.resetDb()
    cy.visit('/tags')
    cy.get('.results').as('results')
  })

  it('Create tag', () => {
    cy.get('@results').find('.add-btn').click()
    cy.get('.add-tag-dialog').as('dialog')
    cy.get('@dialog').find('.accept-btn').as('accept-btn')
    cy.get('@dialog').find('.name-field input').as('name-input')

    cy.url().should('include', '/tags/new')

    // required fields (should fail)
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.required_fields"]')

    // duplicate record (should fail)
    cy.get('@name-input').type('Tag 6')
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.duplicate_record"]')

    // creates a new tag
    cy.get('@name-input').clear().type('new tag')
    cy.get('@accept-btn').click()

    // verifies that the new tag has been added
    cy.get('@results').find('tbody tr:first:contains(new tag)')
  })

  it('Edit tag', () => {
    cy.get('@results').find('tbody tr:first .edit-btn').click()
    cy.get('.edit-tag-dialog').as('dialog')
    cy.get('@dialog').find('.accept-btn').as('accept-btn')
    cy.get('@dialog').find('.name-field input').as('name-input')

    cy.url().should('match', /\/tags\/\d+\/edit/)
    cy.get('@accept-btn').should('not.be.disabled')

    // required fields (should fail)
    cy.get('@name-input').clear()
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.required_fields"]')

    // duplicate record (should fail)
    cy.get('@name-input').type('Tag 5')
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.duplicate_record"]')

    // edit tag
    cy.get('@name-input').clear().type('edited tag')
    cy.get('@accept-btn').click()

    // verifies that the new tag has been added
    cy.get('@results').find('tbody tr:first:contains(edited tag)')
  })

  it('Delete tag', () => {
    cy.get('@results').find('tbody tr:first .delete-btn').click()
    cy.get('.delete-tag-dialog').as('dialog')

    cy.url().should('match', /\/tags\/\d+\/delete/)
    cy.get('@dialog').find('.accept-btn').should('not.be.disabled').click()
    cy.get('@results').find('tbody tr:first:contains(Tag 6)').should('not.exist')
  })
})
