/// <reference path="../support/index.d.ts" />

context('Home page', () => {
  before(cy.login)
  beforeEach(() => {
    cy.resetDb()
  })

  it('Visits dashboard', () => {
    cy.visit('/')
    cy.url().should('include', '/tags/0/domains')
    cy.get('.results')
  })
})
