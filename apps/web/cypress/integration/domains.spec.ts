/// <reference path="../support/index.d.ts" />

context('Create, edit and delete domains', () => {
  before(cy.login)

  beforeEach(() => {
    cy.resetDb()
    cy.visit('/tags/0/domains')
    cy.get('.results').as('results')
  })

  it('Create domain', () => {
    cy.get('@results').find('.add-btn').click()
    cy.get('.add-domain-dialog').as('dialog')
    cy.get('@dialog').find('.accept-btn').as('accept-btn')
    cy.get('@dialog').find('.url-field input').as('url-input')
    cy.get('@dialog').find('.tags-field input').as('tags-input')

    cy.url().should('include', '/tags/0/domains/new')

    // required fields (should fail)
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.required_fields"]')

    // duplicate record (should fail)
    cy.get('@url-input').type('domain8.com')
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.duplicate_record"]')

    // creates a new record
    cy.get('@url-input').clear().type('new.domain.com')
    cy.get('@tags-input').type('tag 1,').type('tag 2,').type('tag 5,')
    cy.get('@accept-btn').click()

    // verifies that the new tag has been added
    cy.get('@results').contains('tbody tr:first', 'new.domain.com').as('row')
    for (const tag of ['tag 1', 'tag 2', 'tag 5']) {
      cy.get('@row').contains(tag, { matchCase: false })
    }
  })

  it('Edit domain', () => {
    cy.get('@results').find('tbody tr:first .edit-btn').click()
    cy.get('.edit-domain-dialog').as('dialog')
    cy.get('@dialog').find('.accept-btn').as('accept-btn')
    cy.get('@dialog').find('.url-field input').as('url-input')
    cy.get('@dialog').find('.tags-field input').as('tags-input')

    cy.url().should('match', /\/tags\/0\/domains\/\d+\/edit/)
    cy.get('@accept-btn').should('not.be.disabled')

    // required fields (should fail)
    cy.get('@url-input').clear()
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.required_fields"]')

    // duplicate record (should fail)
    cy.get('@url-input').type('domain7.com')
    cy.get('@accept-btn').click()
    cy.get('@dialog').find('[class*="errors.duplicate_record"]')

    // edit tag
    cy.get('@url-input').clear().type('edited.domain.com')
    cy.get('@tags-input').type(new Array(10).join('{backspace}')).type('tag 6,').type('tag 5,')
    cy.get('@accept-btn').click()

    // verifies that the new tag has been added
    cy.get('@results').contains('tbody tr:first', 'edited.domain.com').as('row')
    for (const tag of ['tag 6', 'tag 5']) {
      cy.get('@row').contains(tag, { matchCase: false })
    }
  })

  it('Delete domain', () => {
    cy.get('@results').find('tbody tr:first .delete-btn').click()
    cy.get('.delete-domain-dialog').as('dialog')

    cy.url().should('match', /\/tags\/0\/domains\/\d+\/delete/)
    cy.get('@dialog').find('.accept-btn').click()
    cy.get('@results').find('tbody tr:first:contains(domain8.com)').should('not.exist')
  })
})
