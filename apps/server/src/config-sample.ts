export default {
  apiUrl: 'http://localhost:5000', // the "real" API url without version
  apiVersion: 'v1',
  port: '5050',
  timeout: '5s',
  domain: 'domain.com',
  googleClientId: 'xxx.apps.googleusercontent.com',
  secretPhrase: 'Replace with a random secret phrase',
  dbhost: 'localhost',
  dbname: 'semaforo',
  dbuser: 'root',
  dbpass: '[DB PASSWORD]'
}
