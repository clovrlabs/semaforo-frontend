import { Request, Response } from 'express'
import _ from 'lodash'
import * as error from 'src/lib/error'
import * as domain from 'src/queries/domain'

export const getDomains = async (_req: Request, res: Response) => {
  const items = await domain.getDomains()

  return res.json({ items })
}

export const getDomain = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params
  const item = await domain.getDomain(id)

  if (!item) {
    return error.recordNotFound(res)
  }

  return res.json({ item })
}

export const createDomain = async (req: Request<{}, {}, { url: string }>, res: Response) => {
  const { url } = req.body

  if (_.isNil(url)) {
    return error.requiredFields(res, 'URL is required')
  }

  if (await domain.getDomainByUrl(url)) {
    return error.duplicateRecord(res)
  }

  const insertId = await domain.createDomain({ url })
  return res.json({ id: insertId })
}

export const updateDomain = async (
  req: Request<{ id: number }, {}, { url: string }>,
  res: Response
) => {
  const { id } = req.params
  const { url } = req.body

  if (_.isNil(url)) {
    return error.requiredFields(res, 'URL is required')
  }

  if (!(await domain.getDomain(id))) {
    return error.recordNotFound(res)
  }

  const item = await domain.getDomainByUrl(url)
  if (item && item.id != id) {
    return error.duplicateRecord(res)
  }

  await domain.updateDomain(id, { url })
  return res.json()
}

export const deleteDomain = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params

  if (!(await domain.getDomain(id))) {
    return error.recordNotFound(res)
  }

  await domain.deleteDomain(id)
  return res.json()
}

export const getDomainTags = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params

  if (!(await domain.getDomain(id))) {
    return error.recordNotFound(res)
  }

  const items = await domain.getDomainTags(id)
  return res.json({ items })
}

export const updateDomainTags = async (
  req: Request<{ id: number }, {}, { tag_ids: number[] }>,
  res: Response
) => {
  const { id } = req.params
  const { tag_ids: tagIds } = req.body

  if (!(await domain.getDomain(id))) {
    return error.recordNotFound(res)
  }

  const ids = await domain.updateDomainTags(id, { tagIds })
  return res.json({ ids })
}
