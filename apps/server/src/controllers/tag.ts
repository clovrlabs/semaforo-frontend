import { Request, Response } from 'express'
import _ from 'lodash'
import * as error from 'src/lib/error'
import * as tag from 'src/queries/tag'

export const getTags = async (req: Request<{}, {}, {}, { visible?: boolean }>, res: Response) => {
  const { visible } = req.query
  const items = await tag.getTags({ visible })

  return res.json({ items })
}

export const getTagDomains = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params
  const item = await tag.getTag(id)

  if (!item) {
    return error.recordNotFound(res, 'Tag not found')
  }

  const items = await tag.getTagDomains(id)
  return res.json({ items })
}

export const getTag = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params
  const item = await tag.getTag(id)

  if (!item) {
    return error.recordNotFound(res)
  }

  return res.json({ item })
}

export const createTag = async (
  req: Request<{}, {}, { name: string; visible: boolean }>,
  res: Response
) => {
  const { name, visible } = req.body

  if (_.isNil(name) || _.isNil(visible)) {
    return error.requiredFields(res, 'Name and Visible are required')
  }

  if (await tag.getTagByName(name)) {
    return error.duplicateRecord(res)
  }

  const insertId = await tag.createTag({ name, visible })
  return res.json({ id: insertId })
}

export const updateTag = async (
  req: Request<{ id: number }, {}, { name: string; visible: boolean }>,
  res: Response
) => {
  const { id } = req.params
  const { name, visible } = req.body

  if (_.isNil(name) || _.isNil(visible)) {
    return error.requiredFields(res, 'Name and Visible are required')
  }

  if (!(await tag.getTag(id))) {
    return error.recordNotFound(res)
  }

  const item = await tag.getTagByName(name)
  if (item && item.id != id) {
    return error.duplicateRecord(res)
  }

  await tag.updateTag(id, { name, visible })
  return res.json()
}

export const deleteTag = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params

  if (!(await tag.getTag(id))) {
    return error.recordNotFound(res)
  }

  await tag.deleteTag(id)
  return res.json()
}
