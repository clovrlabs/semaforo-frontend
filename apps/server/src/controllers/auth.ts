import { OAuth2Client } from 'google-auth-library'
import { Request, Response } from 'express'
import { RowDataPacket } from 'mysql2'
import jwt from 'jsonwebtoken'
import { conn, toUnnamed } from 'src/lib/db'
import * as gsuite from 'src/lib/gsuite'
import * as error from 'src/lib/error'
import * as user from 'src/queries/user'
import config from 'src/config'

export const login = async (
  req: Request<{}, {}, { id_token: string; access_token: string }>,
  res: Response
) => {
  const { id_token, access_token } = req.body
  const client = new OAuth2Client(config.googleClientId)

  // Verifies the authenticity of the user against the CLIENT_ID.
  const ticket = await client.verifyIdToken({
    idToken: id_token,
    audience: config.googleClientId
  })

  const payload = ticket.getPayload()
  const email = payload?.email
  if (!payload || !email) {
    return error.internalServerError(res, "Couldn't retrieve the identity of the user.")
  }

  const dbuser = await user.getUserByEmail(email)
  const admin = dbuser?.admin ?? false

  const member = await gsuite.getUser(access_token, email)
  const google_admin = member?.isAdmin ?? false

  return res.json({
    token: jwt.sign(
      {
        email,
        admin: admin || google_admin,
        google_admin,
        token: access_token
      },
      config.secretPhrase
    )
  })
}

// TODO: review the getToken controller
// This controller is for testing only.
export const getToken = async (req: Request<{}, {}, { email: string }>, res: Response) => {
  const { email } = req.body

  if (process.env.NODE_ENV != 'development') {
    return error.forbidden(res, 'Dev access only')
  }

  const [[user]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, admin
      from user
      where email = :email`,
      { email }
    )
  )

  const token = jwt.sign(
    {
      user_id: user.id,
      admin: user.admin
    },
    config.secretPhrase
  )

  return Promise.resolve(res.json({ token }))
}
