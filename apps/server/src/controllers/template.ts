import { Request, Response } from 'express'
import _ from 'lodash'
import * as error from 'src/lib/error'
import * as template from 'src/queries/template'

export const getTemplates = async (_req: Request, res: Response) => {
  const items = await template.getTemplates()

  return res.json({ items })
}

export const getTemplate = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params
  const item = await template.getTemplate(id)

  if (!item) {
    return error.recordNotFound(res)
  }

  return res.json({ item })
}

export const createTemplate = async (
  req: Request<{}, {}, { name: string; text: string }>,
  res: Response
) => {
  const { name, text } = req.body

  if (_.isNil(name) || _.isNil(text)) {
    return error.requiredFields(res, 'Name and Text are required')
  }

  if (await template.getTemplateByName(name)) {
    return error.duplicateRecord(res)
  }

  const insertId = await template.createTemplate({ name, text })
  return res.json({ id: insertId })
}

export const updateTemplate = async (
  req: Request<{ id: number }, {}, { name: string; text: string }>,
  res: Response
) => {
  const { id } = req.params
  const { name, text } = req.body

  if (_.isNil(name) || _.isNil(text)) {
    return error.requiredFields(res, 'Name and Text are required')
  }

  if (!(await template.getTemplate(id))) {
    return error.recordNotFound(res)
  }

  const item = await template.getTemplateByName(name)
  if (item && item.id != id) {
    return error.duplicateRecord(res)
  }

  await template.updateTemplate(id, { name, text })
  return res.json()
}

export const deleteTemplate = async (req: Request<{ id: number }>, res: Response) => {
  const { id } = req.params

  if (!(await template.getTemplate(id))) {
    return error.recordNotFound(res)
  }

  await template.deleteTemplate(id)
  return res.json()
}
