import { Request, Response } from 'express'
import _ from 'lodash'
import { http } from 'src/lib/http'

export const getApiKey = async (req: Request, res: Response) => {
  const r = await http(req.token).get('/api-key')

  return res.json(r.data)
}

export const newApiKey = async (req: Request, res: Response) => {
  const r = await http(req.token).post('/api-key')

  return res.json(r.data)
}

export const checkUrl = async (req: Request<{ token: string; url: string }>, res: Response) => {
  const r = await http(req.token).get('/check-url', { params: req.params })

  return res.json(r.data)
}
