import _ from 'lodash'
import { Request, Response } from 'express'
import * as error from 'src/lib/error'
import * as model from 'src/models'
import * as user from 'src/queries/user'

// TODO: users do not longer return `id` (replace by email)
export const getUsers = async (req: Request, res: Response<{ items: model.User[] }>) => {
  const users = await user.getUsers()

  return res.json({
    items: users.map(user => ({
      id: user.id,
      name: user.name,
      email: user.email,
      admin: user.admin,
      email_sent: user?.email_sent,
      last_api_access: user?.last_api_access
    }))
  })
}

export const getUser = async (
  req: Request<{ id: number }>,
  res: Response<{ item: model.User }>
) => {
  const { id } = req.params
  const dbuser = await user.getUser(id)

  if (!dbuser) {
    return error.recordNotFound(res)
  }

  return res.json({
    item: {
      id: dbuser.id,
      name: dbuser.name,
      email: dbuser.email,
      admin: dbuser.admin,
      email_sent: dbuser.email_sent,
      last_api_access: dbuser.last_api_access
    }
  })
}

export const inviteUser = async (
  req: Request<{ id: number }, {}, { subject: string; message: string }>,
  res: Response
) => {
  const { subject, message } = req.body

  if (_.isNil(subject) || _.isNil(message)) {
    return error.requiredFields(res, 'subject and message are required required')
  }

  return Math.random() > 0.5
    ? res.json()
    : error.internalServerError(res, "The message couldn't be sent")
}

export const importUsers = async (req: Request<{ email: string }>, res: Response) => {
  return Promise.resolve(res.json())
}
