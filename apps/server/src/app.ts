import 'express-async-errors'
import express, { Request, Response, NextFunction, Router } from 'express'
import timeout from 'connect-timeout'
import bodyParser from 'body-parser'
import bearerToken from 'express-bearer-token'
import cors from 'cors'
import config from './config'
import { getToken, login } from './controllers/auth'
import { checkUrl, getApiKey, newApiKey } from './controllers/misc'
import { getTags, getTag, getTagDomains, createTag, updateTag, deleteTag } from './controllers/tag'
import {
  getDomains,
  getDomain,
  createDomain,
  updateDomain,
  deleteDomain,
  updateDomainTags,
  getDomainTags
} from './controllers/domain'
import {
  getTemplates,
  getTemplate,
  createTemplate,
  updateTemplate,
  deleteTemplate
} from './controllers/template'
import { getUsers, getUser, inviteUser, importUsers } from './controllers/user'

const haltOnTimedout = (req: Request, _: Response, next: NextFunction) => {
  !req.timedout && next()
}

function bodyTrimmer(req: Request, res: Response, next: NextFunction) {
  if (['POST', 'PATCH', 'UPDATE'].includes(req.method)) {
    for (const [key, value] of Object.entries(req.body)) {
      if (typeof value == 'string') {
        req.body[key] = value.trim() || null
      }
    }
  }
  next()
}

/**
 * Routes.
 */
const router = Router()
router
  // auth
  .post('/login', login)
  .post('/token', getToken)
  // misc
  .get('/check-url', checkUrl)
  .get('/api-key', getApiKey)
  .post('/api-key', newApiKey)
  // tags
  .get('/tags', getTags)
  .get('/tags/:id', getTag)
  .get('/tags/:id/domains', getTagDomains)
  .post('/tags/', createTag)
  .patch('/tags/:id', updateTag)
  .delete('/tags/:id', deleteTag)
  // domains
  .get('/domains', getDomains)
  .get('/domains/:id', getDomain)
  .post('/domains/', createDomain)
  .patch('/domains/:id', updateDomain)
  .delete('/domains/:id', deleteDomain)
  .get('/domains/:id/tags', getDomainTags)
  .patch('/domains/:id/tags', updateDomainTags)
  // templates
  .get('/templates', getTemplates)
  .get('/templates/:id', getTemplate)
  .post('/templates/', createTemplate)
  .patch('/templates/:id', updateTemplate)
  .delete('/templates/:id', deleteTemplate)
  // users
  .get('/users', getUsers)
  .get('/users/:id', getUser)
  .post('/users/import', importUsers)
  .post('/users/:id/invite', inviteUser)

/**
 * Application.
 */
express()
  .set('json spaces', 2)
  .use(cors())
  .use(bodyParser.json())
  .use(bodyTrimmer)
  .use(bearerToken())
  .use(timeout(config.timeout))
  .use((_, res, next) => {
    res.setHeader('Content-Type', 'application/json')
    next()
  })
  .use(haltOnTimedout)
  .use(`/${config.apiVersion}`, router)
  .listen(config.port, () => console.log(`Listening on port ${config.port}!`))
