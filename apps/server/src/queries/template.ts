import { RowDataPacket, ResultSetHeader } from 'mysql2'
import _ from 'lodash'
import * as model from 'src/models'
import { conn, toUnnamed } from 'src/lib/db'

export const getTemplates = async (): Promise<model.Template[]> => {
  const [rows] = await conn.query<RowDataPacket[]>(
    `
    select id, name, text
    from template
    order by id desc`
  )

  return rows.map(({ id, name, text }) => ({ id, name, text }))
}

export const getTemplate = async (id: number): Promise<model.Template | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, name, text
      from template
      where id = :id`,
      { id }
    )
  )
  if (!row) {
    return undefined
  }

  return { id: row.id, name: row.name, text: row.text }
}

export const getTemplateByName = async (name: string): Promise<model.Template | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, name, text
      from template
      where name = :name`,
      { name }
    )
  )
  if (!row) {
    return undefined
  }

  return { id: row.id, name: row.name, text: row.text }
}

export const createTemplate = async (params: { name: string; text: string }): Promise<number> => {
  const { name, text } = params

  const [{ insertId }] = await conn.execute<ResultSetHeader>(
    ...toUnnamed(
      `
      insert into template(name, text)
      values(:name, :text)`,
      { name, text }
    )
  )

  return insertId
}

export const updateTemplate = async (
  id: number,
  params: { name: string; text: string }
): Promise<void> => {
  const { name, text } = params

  await conn.execute(
    ...toUnnamed(
      `
      update template
      set name = :name, text = :text
      where id = :id`,
      { id, name, text }
    )
  )
}

export const deleteTemplate = async (id: number): Promise<void> => {
  await conn.execute(
    ...toUnnamed(
      `
      delete from template
      where id = :id`,
      { id }
    )
  )
}
