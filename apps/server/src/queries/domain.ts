import { RowDataPacket, ResultSetHeader } from 'mysql2'
import _ from 'lodash'
import { conn, toUnnamed } from 'src/lib/db'
import * as model from 'src/models'

export const getDomains = async (): Promise<(model.Domain & { tags: model.Tag[] })[]> => {
  const [rows] = await conn.query<RowDataPacket[]>(
    `
    select
      d.id,
      d.url,
      group_concat(t.id) as tag_ids,
      group_concat(t.name) as tag_names,
      group_concat(t.visible) as tag_visibles
    from domain as d
    left join tag_domain as td
      on td.domain_id = d.id
    left join tag as t
      on t.id = td.tag_id
    group by d.id
    order by d.id desc`
  )

  return rows.map(row => {
    const ids = row.tag_ids?.split(',') || []
    const names = row.tag_names?.split(',') || []
    const visibles = row.tag_visibles?.split(',') || []
    const tags = _.zipWith(ids, names, visibles, (id, name, visible) => ({
      id: parseInt(`${id}`, 10),
      name: `${name}`,
      visible: parseInt(`${visible}`, 10) > 0
    }))

    return {
      id: parseInt(`${row.id}`, 10),
      url: row.url,
      tags
    }
  })
}

export const getDomain = async (id: number): Promise<model.Domain | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, url
      from domain
      where id = :id`,
      { id }
    )
  )
  if (!row) {
    return undefined
  }

  return {
    id: row.id,
    url: row.url
  }
}

export const getDomainByUrl = async (url: string): Promise<model.Domain | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, url
      from domain
      where url = :url`,
      { url }
    )
  )
  if (!row) {
    return undefined
  }

  return {
    id: row.id,
    url: row.url
  }
}

export const createDomain = async (params: { url: string }) => {
  const { url } = params
  const [{ insertId }] = await conn.execute<ResultSetHeader>(
    ...toUnnamed(
      `
      insert into domain(url)
      values(:url)`,
      { url }
    )
  )

  return insertId
}

export const updateDomain = async (id: number, params: { url: string }): Promise<void> => {
  const { url } = params

  await conn.execute(
    ...toUnnamed(
      `
      update domain
      set url = :url
      where id = :id`,
      { id, url }
    )
  )
}

export const deleteDomain = async (id: number): Promise<void> => {
  await conn.execute(
    ...toUnnamed(
      `
      delete from domain
      where id = :id`,
      { id }
    )
  )
}

export const getDomainTags = async (id: number): Promise<model.Tag[]> => {
  const [rows] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select t.id, t.name, t.visible
      from tag as t
      inner join tag_domain as td
        on td.tag_id = t.id
      where td.domain_id = :id`,
      { id }
    )
  )

  return rows.map(({ id, name, visible }) => ({ id, name, visible }))
}

export const updateDomainTags = async (
  id: number,
  params: { tagIds: number[] }
): Promise<number[]> => {
  const { tagIds } = params

  await conn.execute<ResultSetHeader>(
    ...toUnnamed(
      `
      delete from tag_domain
      where domain_id = :id`,
      { id }
    )
  )

  const items = await Promise.all(
    tagIds.map(tagId =>
      conn.execute<ResultSetHeader>(
        ...toUnnamed(
          `
            insert into tag_domain(tag_id, domain_id)
            values(:tagId, :id)`,
          { tagId, id }
        )
      )
    )
  )

  return items.map(([{ insertId }]) => insertId)
}
