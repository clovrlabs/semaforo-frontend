import _ from 'lodash'
import { RowDataPacket } from 'mysql2'
import * as model from 'src/models'
import { conn, toUnnamed } from 'src/lib/db'

export const getUsers = async (): Promise<model.User[]> => {
  const [rows] = await conn.query<RowDataPacket[]>(
    `
    select id, email, name, admin, email_sent, last_api_access
    from user
    order by id desc`
  )

  return rows.map(row => ({
    id: row.id,
    email: row.email,
    name: row.name,
    admin: row.admin,
    email_sent: row.email_sent,
    last_api_access: row.last_api_access
  }))
}

export const getUser = async (id: number): Promise<model.User | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, email, name, admin, email_sent, last_api_access
      from user
      where id = :id`,
      { id }
    )
  )
  if (!row) {
    return undefined
  }

  return {
    id: row.id,
    email: row.email,
    name: row.name,
    admin: row.admin,
    email_sent: row.email_sent,
    last_api_access: row.last_api_access
  }
}

export const getUserByEmail = async (email: string): Promise<model.User | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, email, name, admin, email_sent, last_api_access
      from user
      where email = :email`,
      { email }
    )
  )
  if (!row) {
    return undefined
  }

  return {
    id: row.id,
    email: row.email,
    name: row.name,
    admin: row.admin,
    email_sent: row.email_sent,
    last_api_access: row.last_api_access
  }
}