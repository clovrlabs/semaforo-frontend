import { RowDataPacket, ResultSetHeader } from 'mysql2'
import _ from 'lodash'
import * as model from 'src/models'
import { conn, toUnnamed } from 'src/lib/db'

export const getTags = async (filter: { visible?: boolean }): Promise<model.Tag[]> => {
  const { visible } = filter

  const [rows] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, name, visible
      from tag
      where if(:visible is null, true, visible = :visible)
      order by id desc`,
      { visible }
    )
  )

  return rows.map(({ id, name, visible }) => ({ id, name, visible }))
}

export const getTagDomains = async (
  id: number
): Promise<(model.Domain & { tags: model.Tag[] })[]> => {
  const [rows] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select
        d.id,
        d.url,
        group_concat(t.id) as tag_ids,
        group_concat(t.name) as tag_names,
        group_concat(t.visible) as tag_visibles
      from domain as d
      inner join tag_domain as td
        on td.domain_id = d.id
      inner join tag_domain as td1
        on td1.domain_id = d.id
      inner join tag as t
        on t.id = td1.tag_id
      where td.tag_id = :id
      group by d.id
      order by d.id desc`,
      { id }
    )
  )

  return rows.map(row => {
    const ids = row.tag_ids.split(',') || []
    const names = row.tag_names.split(',') || []
    const visibles = row.tag_visibles.split(',') || []
    const tags = _.zipWith(ids, names, visibles, (id, name, visible) => ({
      id: parseInt(`${id}`, 10),
      name: `${name}`,
      visible: parseInt(`${visible}`, 10) > 0
    }))

    return { id: row.id, url: row.url, tags }
  })
}

export const getTag = async (id: number): Promise<model.Tag | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, name, visible
      from tag
      where id = :id`,
      { id }
    )
  )
  if (!row) {
    return undefined
  }

  return { id: row.id, name: row.name, visible: row.visible }
}

export const getTagByName = async (name: string): Promise<model.Tag | undefined> => {
  const [[row]] = await conn.query<RowDataPacket[]>(
    ...toUnnamed(
      `
      select id, name, visible
      from tag
      where name = :name`,
      { name }
    )
  )
  if (!row) {
    return undefined
  }

  return { id: row.id, name: row.name, visible: row.visible }
}

export const createTag = async (params: { name: string; visible: boolean }): Promise<number> => {
  const { name, visible } = params

  const [{ insertId }] = await conn.execute<ResultSetHeader>(
    ...toUnnamed(
      `
      insert into tag(name, visible)
      values(:name, :visible)`,
      { name, visible }
    )
  )

  return insertId
}

export const updateTag = async (
  id: number,
  params: { name: string; visible: boolean }
): Promise<void> => {
  const { name, visible } = params

  await conn.execute(
    ...toUnnamed(
      `
      update tag
      set name = :name, visible = :visible
      where id = :id`,
      { id, name, visible }
    )
  )
}

export const deleteTag = async (id: number): Promise<void> => {
  await conn.execute(
    ...toUnnamed(
      `
      delete from tag
      where id = :id`,
      { id }
    )
  )
}
