declare module 'named-placeholders' {
  function namedPlaceholders(): (sql: string, args: { [key: string]: any }) => [string, any[]]
  export = namedPlaceholders
}
