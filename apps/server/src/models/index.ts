export type Domain = {
  id: number
  url: string
}

export type Tag = {
  id: number
  name: string
  visible: boolean
}

export type Template = {
  id: number
  name: string
  text: string
}

export type User = {
  id: number
  email: string
  name: string
  admin: boolean
  email_sent?: string
  last_api_access?: string
}
