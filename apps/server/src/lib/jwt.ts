import jwt from 'jsonwebtoken'

export const parseToken = (token: string | undefined): { token: string } => {
  return jwt.decode(token || '') as any
}
