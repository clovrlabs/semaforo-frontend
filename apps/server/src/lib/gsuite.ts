import axios from 'axios'

export const getUsers = async (token: string | undefined, params: { domain: string }) => {
  const { domain } = params
  const r = await axios.get<{
    users: {
      id: string
      primaryEmail: string
      name: {
        givenName: string
        familyName: string
        fullName: string
      }
      isAdmin: boolean
    }[]
  }>('https://www.googleapis.com/admin/directory/v1/users', {
    params: { access_token: token, viewType: 'admin_view', domain }
  })

  return r.data.users
}

export const getUser = async (token: string, email: string) => {
  const r = await axios.get<{
    id: string
    primaryEmail: string
    name: {
      givenName: string
      familyName: string
      fullName: string
    }
    isAdmin: boolean
    emails: { address: string; primary?: boolean }[]
  }>(`https://www.googleapis.com/admin/directory/v1/users/${email}`, {
    params: { access_token: token, viewType: 'admin_view' }
  })

  return r.data
}
