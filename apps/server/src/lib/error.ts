import { Response } from 'express'

enum Status {
  BadRequest = 400,
  Unauthorized = 401,
  Forbidden = 403,
  NotFound = 404,
  InternalServerError = 500
}

export type HttpError = {
  code: string
  message?: string
}

export const badRequest = (res: Response, message?: string) => {
  return res.status(Status.BadRequest).send({ code: 'bad_request', message })
}

export const unauthorized = (res: Response, message?: string) => {
  return res.status(Status.Unauthorized).send({ code: 'unauthorized', message })
}

export const forbidden = (res: Response, message?: string) => {
  return res.status(Status.Forbidden).send({ code: 'forbidden', message })
}

export const notFound = (res: Response, message?: string) => {
  return res.status(Status.NotFound).send({ code: 'not_found', message })
}

export const internalServerError = (res: Response, message?: string) => {
  return res.status(Status.InternalServerError).send({ code: 'internal_server_error', message })
}

export const duplicateRecord = (res: Response, message?: string) => {
  return res.status(Status.BadRequest).send({ code: 'duplicate_record', message })
}

export const requiredFields = (res: Response, message?: string) => {
  return res.status(Status.BadRequest).send({ code: 'required_fields', message })
}

export const recordNotFound = (res: Response, message?: string) => {
  return res.status(Status.NotFound).send({ code: 'record_not_found', message })
}
