import mysql from 'mysql2'
import namedPlaceholders from 'named-placeholders'
import config from 'src/config'

const unnamed = namedPlaceholders()
export const toUnnamed = (sql: string, args: { [key: string]: any } = {}): [string, any[]] => {
  const [_sql, _args] = unnamed(sql, args)

  return [_sql, _args.map(x => (x === undefined ? null : x))]
}

// In theory `pool` is a better choose than `conn`. In practice `pool` leads to multiple errors
// related to `connectionLimit`. When MySQL reaches `connectionLimit`, it no longer responds.
export const pool = mysql
  .createPool({
    host: config.dbhost,
    database: config.dbname,
    user: config.dbuser,
    password: config.dbpass,
    connectionLimit: 100
  })
  .promise()

export const conn = mysql
  .createConnection({
    host: config.dbhost,
    database: config.dbname,
    user: config.dbuser,
    password: config.dbpass
  })
  .promise()
