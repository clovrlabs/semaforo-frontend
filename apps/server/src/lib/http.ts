import axios from 'axios'
import config from 'src/config'

export const http = (token?: string) => {
  const { apiUrl, apiVersion } = config

  return axios.create({
    baseURL: `${apiUrl}/${apiVersion}`,
    headers: {
      ...(token && { Authorization: `Bearer ${token}` })
    }
  })
}
