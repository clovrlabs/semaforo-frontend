-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `domain`;
CREATE TABLE `domain` (
  `id` int NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `url` (`url`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tag`;
CREATE TABLE `tag` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `visible` tinyint NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `tag_domain`;
CREATE TABLE `tag_domain` (
  `id` tinyint NOT NULL AUTO_INCREMENT,
  `tag_id` int NOT NULL,
  `domain_id` int NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `tag_id_domain_id` (`tag_id`,`domain_id`),
  KEY `tag_id` (`tag_id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `template`;
CREATE TABLE `template` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organization_id` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_api_access` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `organization_id` (`email`),
  CONSTRAINT `user_chk_1` CHECK ((`admin` in (0,1)))
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

TRUNCATE `domain`;
INSERT INTO `domain` (`id`, `url`) VALUES
(29,	'domain1.com'),
(30,	'domain2.com'),
(31,	'domain3.com'),
(32,	'domain4.com'),
(33,	'domain5.com'),
(41,	'domain6.com'),
(42,	'domain7.com'),
(58,	'domain8.com');

TRUNCATE `tag`;
INSERT INTO `tag` (`id`, `name`, `visible`) VALUES
(1,	'Tag 1',	1),
(2,	'Tag 2',	1),
(3,	'Tag 3',	0),
(4,	'Tag 4',	0),
(5,	'Tag 5',	0),
(85,	'Tag 6',	1);

TRUNCATE `tag_domain`;
INSERT INTO `tag_domain` (`id`, `tag_id`, `domain_id`) VALUES
(45,	1,	30),
(3,	1,	31),
(15,	1,	50),
(19,	1,	51),
(24,	1,	52),
(74,	2,	29),
(4,	2,	32),
(41,	2,	33),
(23,	2,	52),
(75,	3,	29),
(44,	3,	30),
(40,	3,	33),
(14,	3,	50),
(18,	3,	51),
(54,	3,	57),
(59,	3,	58),
(13,	4,	50),
(17,	4,	51),
(22,	4,	52),
(49,	4,	55),
(53,	4,	57),
(58,	4,	58),
(16,	5,	51),
(48,	5,	55),
(20,	63,	51),
(21,	64,	51),
(25,	65,	52),
(26,	66,	52),
(46,	70,	53),
(47,	77,	55),
(50,	78,	55),
(51,	79,	55),
(52,	82,	57),
(55,	83,	57),
(56,	84,	57),
(57,	85,	58),
(60,	86,	58),
(61,	87,	58);

TRUNCATE `template`;
INSERT INTO `template` (`id`, `name`, `text`) VALUES
(2,	'Template 2',	'<p>aaa bbb ccc...</p>'),
(4,	'Template 3',	'<p>Lorem ipsum</p><p>dolor sit<br>amet.</p>'),
(8,	'edited template',	'...Lorem ipsum dolor...');

TRUNCATE `user`;
INSERT INTO `user` (`id`, `email`, `admin`, `name`, `last_api_access`) VALUES
(1,	'owner@gonzaloch.com',	1,	'Gonzalo Chumillas',	NULL);

-- 2020-12-25 16:43:25
