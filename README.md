The purpose of Semaforo is to guarantee a safe web browsing. The project consists of a Web Application, two extensions for Chrome and Firefox and an API service. The application keeps a list of "safe domains" and each time the user visits a website, the extension communicates to the API to verify that the URL is "safe".

## Installation

Download the repository and change to the `develop` branch:

```bash
git clone https://gitlab.com/clovrlabs/semaforo-frontend
cd semaforo-frontend
git checkout develop
```

Install libraries:

```bash
yarn
```

Copy `apps/web/public/config-example.toml` to `.apps/web/public/config.toml` and change the current variables properly:

```toml
api_url = "https://path/to/api"

[google]
client_id = "xxx-yyy.apps.googleusercontent.com"

```

Start the development server:

```bash
# started at
# http://localhost:3000
yarn web:start
```

And don't forget to start the backend server in first place.

## Available Scripts

### `yarn web:start`

Runs the app in the development mode.

### `yarn server:start`

Runs the "Auxiliary server" to test the web app.

### `yarn cypress:open`

Opens the [Cypress](https://www.cypress.io/) testing tool.

### `yarn cypress:run`

Runs all [Cypress](https://www.cypress.io/) tests without opening the UI.

### `yarn build`

Builds the app for production to the `build` folder..
