// TODO: these entities are not shareable. move to apps/web
import { DateTime } from 'luxon'

export type Domain = {
  id: number
  url: string
}

export type Tag = {
  id: number
  name: string
  visible: boolean
}

export type Template = {
  id: number
  name: string
  text: string
}

export type User = {
  id: number
  email: string
  name: string
  admin: boolean
  emailSent?: DateTime
  lastApiAccess?: DateTime
}
