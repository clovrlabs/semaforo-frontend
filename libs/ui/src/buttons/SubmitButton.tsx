// Submit button.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import {
  KeyboardArrowRight as KeyboardArrowRightIcon,
  HourglassEmpty as HourglassEmptyIcon
} from '@material-ui/icons'
import { Button, ButtonProps } from '@material-ui/core'

export type SubmitButtonsProps = ButtonProps & {
  loading?: boolean
}

export default ({ type = 'submit', disabled, loading, children, ...rest }: SubmitButtonsProps) => (
  <Button type={type} disabled={disabled || loading} {...rest}>
    {children}
    {loading ? <HourglassEmptyIcon /> : <KeyboardArrowRightIcon />}
  </Button>
)
