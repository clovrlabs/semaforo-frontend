// Paginator component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import clsx from 'clsx'
import { Button, ButtonGroup, Box, Typography } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import {
  FirstPage as FirstIcon,
  LastPage as LastIcon,
  NavigateBefore as PrevIcon,
  NavigateNext as NextIcon
} from '@material-ui/icons'
import { context } from './context'

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  selectedBtn: {
    backgroundColor: theme.palette.action.selected
  },
  hidden: {
    display: 'none'
  }
}))

export type PaginatorProps = {
  page: number
  numButtons?: number
  onPageChange: (page: number) => void
}

export default ({ page: value, numButtons = 3, onPageChange }: PaginatorProps) => {
  const { rows, rowsPerPage } = React.useContext(context)
  const classes = useStyles()
  const [offset, setOffset] = React.useState(0)
  const numPages = React.useMemo(
    () => Math.floor(rows.length / rowsPerPage) + Number(rows.length % rowsPerPage > 0),
    [rows, rowsPerPage]
  )
  const page0 = React.useMemo(() => Math.max(0, offset), [offset])
  const page1 = React.useMemo(() => Math.max(page0, Math.min(numPages, offset + numButtons) - 1), [
    offset,
    page0,
    numPages,
    numButtons
  ])
  const page = React.useMemo(() => Math.max(0, Math.min(numPages - 1, value)), [value, numPages])
  const hideArrowButtons = numButtons < numPages

  React.useEffect(() => {
    if (page < offset) {
      setOffset(page)
    } else if (page > page1) {
      setOffset(page - numButtons + 1)
    }
  }, [page, offset, page1, numButtons])

  return (
    <Box className={clsx(classes.container, { [classes.hidden]: numPages <= 1 })}>
      {/* TODO: use translations */}
      <Typography>
        Page {page + 1} of {numPages}
      </Typography>
      <ButtonGroup size="medium">
        {hideArrowButtons && [
          <Button key={0} disabled={page <= 0} onClick={() => onPageChange(0)}>
            <FirstIcon />
          </Button>,
          <Button key={1} disabled={page <= 0} onClick={() => onPageChange(page - 1)}>
            <PrevIcon />
          </Button>
        ]}
        {_.range(page0, page1 + 1).map(value => (
          <Button
            key={value}
            className={clsx({ [classes.selectedBtn]: value == page })}
            onClick={() => onPageChange(value)}
          >
            {value + 1}
          </Button>
        ))}
        {hideArrowButtons && [
          <Button key={0} disabled={page >= numPages - 1} onClick={() => onPageChange(page + 1)}>
            <NextIcon />
          </Button>,
          <Button
            key={1}
            disabled={page >= numPages - 1}
            onClick={() => onPageChange(numPages - 1)}
          >
            <LastIcon />
          </Button>
        ]}
      </ButtonGroup>
    </Box>
  )
}
