// DataTable component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import clsx from 'clsx'
import {
  Paper,
  Checkbox,
  IconButton,
  TableContainer,
  Table,
  TableRow,
  TableHead,
  TableBody,
  TableCell
} from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { context } from './context'
import Paginator from './Paginator'

const useStyles = makeStyles(theme => ({
  tableContainer: {
    marginBottom: theme.spacing(2)
  },
  shrink: {
    width: 1
  }
}))

export type Row = {
  [key: string]: any
}

export type Column = {
  name: string
  title?: string
  shrink?: boolean
  whiteSpace?: 'normal' | 'nowrap' | 'pre' | 'pre-line' | 'pre-wrap' | 'initial' | 'inherit'
  align?: 'left' | 'center' | 'right' | 'justify'
}

export type HeaderAction = {
  icon: React.ReactNode
  onClick: () => void
  className?: string
  disabled?: boolean
  title?: string
  color?: string
}

export type RowAction = {
  icon: React.ReactNode
  onClick: (row: Row) => void
  className?: string
  disabled?: boolean
  visible?: boolean
  title?: string
  color?: string
}

export type DataTableProps = {
  columns: Column[]
  rows: Row[]
  className?: string
  primaryKey?: string
  headerActions?: HeaderAction[]
  rowActions?: RowAction[] | ((row: Row) => RowAction[])
  rowRenders?: { [name: string]: (row: Row) => React.ReactNode }
  rowsPerPage?: number
  selection?: boolean
  selectedRows?: Row[]
  onSelectedRowsChange?: (rows: Row[]) => void
}

export const DataTable = ({
  className,
  columns,
  rows,
  primaryKey = 'id',
  headerActions = [],
  rowActions = () => [],
  rowRenders = {},
  rowsPerPage = 10,
  selection = false,
  selectedRows: _selectedRows,
  onSelectedRowsChange
}: DataTableProps) => {
  const classes = useStyles()
  const [page, setPage] = React.useState(0)
  const [selectedRows, setSelectedRows] = React.useState<Row[]>(_selectedRows || [])
  const pageRows = rows.slice(rowsPerPage * page, rowsPerPage * (page + 1))
  const colSpan = Math.max(headerActions.length, rowActions.length)

  const onToggleAll = () => {
    setSelectedRows(selectedRows.length > 0 ? [] : rows)
  }

  const onToggleRow = (row: Row) => {
    setSelectedRows(
      selectedRows.includes(row) ? [...selectedRows.filter(x => x !== row)] : [...selectedRows, row]
    )
  }

  React.useEffect(() => {
    setSelectedRows([])
  }, [rows])

  React.useEffect(() => {
    onSelectedRowsChange && onSelectedRowsChange(selectedRows)
  }, [selectedRows, onSelectedRowsChange])

  return (
    <context.Provider value={{ rows, rowsPerPage }}>
      <TableContainer component={Paper} className={clsx([className, classes.tableContainer])}>
        <Table>
          <TableHead>
            <TableRow>
              {selection && (
                <TableCell padding="checkbox" className={classes.shrink}>
                  <Checkbox
                    indeterminate={selectedRows.length > 0 && selectedRows.length < rows.length}
                    disabled={rows.length == 0}
                    checked={rows.length > 0 && rows.every(row => selectedRows.includes(row))}
                    onChange={onToggleAll}
                  />
                </TableCell>
              )}
              {columns.map((col, i) => {
                const cols = Math.max(0, colSpan - headerActions.length + 1)
                return (
                  <TableCell
                    key={i}
                    className={clsx({ [classes.shrink]: col.shrink })}
                    style={{ textAlign: col.align }}
                    {...(i == columns.length - 1 && { colSpan: cols })}
                  >
                    {col.title || col.name}
                  </TableCell>
                )
              })}
              {headerActions.map((action, j) => (
                <TableCell key={j} align="right" className={classes.shrink}>
                  <IconButton
                    className={action.className}
                    size="small"
                    disabled={action.disabled}
                    color={action.color as any}
                    title={action.title}
                    onClick={() => action.onClick()}
                  >
                    {action.icon}
                  </IconButton>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {pageRows.map(row => (
              <TableRow key={row[primaryKey]}>
                {selection && (
                  <TableCell padding="checkbox">
                    <Checkbox
                      checked={selectedRows.includes(row)}
                      onChange={() => onToggleRow(row)}
                    />
                  </TableCell>
                )}
                {columns.map((col, j) => {
                  const cols = Math.max(0, colSpan - rowActions.length + 1)
                  const render = rowRenders[col.name]
                  return (
                    <TableCell
                      key={j}
                      style={{ whiteSpace: col.whiteSpace, textAlign: col.align }}
                      {...(j == columns.length - 1 && { colSpan: cols })}
                    >
                      {render ? render(row) : row[col.name]}
                    </TableCell>
                  )
                })}
                {(Array.isArray(rowActions) ? rowActions : rowActions(row)).map((action, j) => (
                  <TableCell key={j} align="right" className={classes.shrink}>
                    {(action.visible === undefined || action.visible) && (
                      <IconButton
                        className={action.className}
                        size="small"
                        disabled={action.disabled}
                        color={action.color as any}
                        title={action.title}
                        onClick={() => action.onClick(row)}
                      >
                        {action.icon}
                      </IconButton>
                    )}
                  </TableCell>
                ))}
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Paginator page={page} onPageChange={setPage} />
    </context.Provider>
  )
}

export default DataTable
