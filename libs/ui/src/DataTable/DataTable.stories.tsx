// DataTable stories.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import { loremIpsum } from 'lorem-ipsum'
import { Box } from '@material-ui/core'
import { Refresh as RefreshIcon, Delete as DeleteIcon, Edit as EditIcon } from '@material-ui/icons'
import { makeStyles } from '@material-ui/core/styles'
import { Story, Meta } from '@storybook/react/types-6-0'
import DataTable, { DataTableProps, Column, Row, RowAction, HeaderAction } from './DataTable'

export default {
  title: 'DataTable',
  component: DataTable,
  argTypes: {
    selection: {
      control: {
        type: 'boolean'
      },
      defaultValue: true
    },
    rowsPerPage: {
      control: {
        type: 'select',
        options: [3, 5, 10, 25]
      },
      defaultValue: 3
    }
  }
} as Meta

const useStyles = makeStyles(theme => ({
  box: {
    maxWidth: theme.breakpoints.values.md
  }
}))

const columns: Column[] = [
  { name: 'id', title: 'ID', shrink: true },
  { name: 'name', title: 'Name', whiteSpace: 'nowrap' },
  { name: 'text', title: 'Text' }
]

const headerActions: HeaderAction[] = [
  {
    icon: <RefreshIcon />,
    title: 'Refresh table',
    // eslint-disable-next-line
    onClick: () => window.alert(`Refreshing table`)
  }
]

const rowActions: RowAction[] = [
  {
    icon: <DeleteIcon />,
    title: 'Delete record',
    color: 'secondary',
    // eslint-disable-next-line
    onClick: row => window.alert(`Deleting record ${row.id}`)
  },
  {
    icon: <EditIcon />,
    title: 'Edit record',
    // eslint-disable-next-line
    onClick: row => window.alert(`Editing record ${row.id}`)
  }
]

const rows = _.range(13).map(i => ({
  id: i,
  name: `Row ${i}`,
  text: loremIpsum({
    count: _.random(1) + 1,
    sentenceLowerBound: 2,
    sentenceUpperBound: 4,
    units: 'paragraphs',
    suffix: '\n\n'
  })
}))

export const Basic: Story<DataTableProps> = ({ selection, rowsPerPage }) => {
  const classes = useStyles()
  const [selectedRows, setSelectedRows] = React.useState<Row[]>([])

  return (
    <Box className={classes.box}>
      <DataTable
        rows={rows}
        columns={columns}
        rowsPerPage={rowsPerPage}
        selection={selection}
        headerActions={headerActions}
        rowActions={rowActions}
        rowRenders={{
          text: row => <div style={{ whiteSpace: 'pre-line' }}>{row.text}</div>
        }}
        selectedRows={selectedRows}
        onSelectedRowsChange={setSelectedRows}
      />
    </Box>
  )
}
