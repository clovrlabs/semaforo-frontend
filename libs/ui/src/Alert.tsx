// Alert notifications.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import { Snackbar, SnackbarProps, IconButton } from '@material-ui/core'
import { Close as CloseIcon } from '@material-ui/icons'

export type AlertProps = SnackbarProps & {
  autoHide?: boolean
}

export const Alert = ({
  onClose,
  autoHide = true,
  autoHideDuration = 2000,
  anchorOrigin = { vertical: 'bottom', horizontal: 'center' },
  ...rest
}: AlertProps) => (
  <Snackbar
    anchorOrigin={anchorOrigin}
    autoHideDuration={autoHide ? autoHideDuration : undefined}
    onClose={onClose}
    action={
      !autoHide && (
        <IconButton size="small" color="inherit" onClick={e => onClose && onClose(e, 'clickaway')}>
          <CloseIcon fontSize="small" />
        </IconButton>
      )
    }
    {...rest}
  />
)

export default Alert
