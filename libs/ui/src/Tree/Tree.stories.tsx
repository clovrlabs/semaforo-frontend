// Tree story.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import { Paper } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { Story, Meta } from '@storybook/react/types-6-0'
import Tree, { TreeProps, TreeItem } from './Tree'

export default {
  title: 'Tree',
  component: Tree
} as Meta

const useStyles = makeStyles(() => ({
  box: {
    maxWidth: 400
  }
}))

const buildItems = (depth: number, parentIds: number[] = []): TreeItem[] =>
  depth > 0
    ? _.range(depth).map(i => {
        const ids = [...parentIds, i]
        return {
          id: ids.join(''),
          name: `Item ${ids.map(x => x).join('')}`,
          items: buildItems(depth - i - 1, ids)
        }
      })
    : []

export const Basic: Story<TreeProps> = () => {
  const classes = useStyles()
  const [selectedItemId, setSelectedItemId] = React.useState<React.ReactText>()
  const items = React.useMemo(() => buildItems(4), [])

  return (
    <Paper className={classes.box}>
      <Tree
        items={items}
        selectedItemId={selectedItemId}
        onSelectItem={item => setSelectedItemId(item.id)}
      />
    </Paper>
  )
}
