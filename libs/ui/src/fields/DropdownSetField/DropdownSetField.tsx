// DropdownSetField component.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import clsx from 'clsx'
import { Popover, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import { ArrowDropDown as DropdownIcon } from '@material-ui/icons'
import SetField, { SetFieldProps } from '../SetField'
import { CheckedIcon, UncheckedIcon } from './icons'

const useStyles = makeStyles(theme => ({
  popoverPaper: {
    marginTop: theme.spacing(2)
  },
  listItemIcon: {
    minWidth: 'auto',
    marginRight: theme.spacing(1.5)
  },
  inputContainer: {
    position: 'relative'
  },
  dropdownButton: {
    position: 'absolute',
    right: 0,
    bottom: 8,
    padding: '8px 0 8px 16px',
    cursor: 'pointer',
    color: theme.palette.text.secondary
  },
  dropdownIcon: {
    float: 'left'
  }
}))

export type DropdownSetFieldProps = SetFieldProps & {
  options: React.ReactText[]
}

export default ({
  value,
  onChange,
  className,
  options,
  caseSensitive = false,
  ...rest
}: DropdownSetFieldProps) => {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState<HTMLElement | null>(null)
  const inputRef = React.useRef(null)

  const equal = (v1: React.ReactText, v2: React.ReactText) => {
    const [a, b] = [`${v1}`, `${v2}`]
    return caseSensitive ? a == b : _.toLower(a) == _.toLower(b)
  }

  const items = React.useMemo(
    () => options.map(name => ({ checked: value.some(x => equal(x, name)), name })),
    [options, value]
  )

  const handleSelectItem = (option: React.ReactText) => {
    onChange &&
      onChange(value.includes(option) ? value.filter(x => !equal(x, option)) : [...value, option])
  }

  const handleChange = (value: React.ReactText[]) => {
    onChange && onChange(value.map(x => options.find(y => equal(x, y)) || x))
  }

  return (
    <div className={className}>
      <div ref={inputRef} className={classes.inputContainer}>
        <SetField value={value} onChange={handleChange} caseSensitive={caseSensitive} {...rest} />
        <div
          className={clsx(['dropdown-btn', classes.dropdownButton])}
          onClick={() => setAnchorEl(inputRef.current)}
        >
          <DropdownIcon className={classes.dropdownIcon} style={{}} />
        </div>
      </div>
      <Popover
        className={className ? `${className}-options` : undefined}
        open={!!anchorEl}
        anchorEl={anchorEl}
        PaperProps={{
          style: {
            // @ts-ignore
            width: inputRef.current?.clientWidth || undefined
          }
        }}
        classes={{ paper: classes.popoverPaper }}
        anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
        transformOrigin={{ vertical: 'top', horizontal: 'left' }}
        onClose={() => setAnchorEl(null)}
        disableRestoreFocus
      >
        <List>
          {items.map(item => (
            <ListItem
              className="item"
              key={item.name}
              button
              onClick={() => handleSelectItem(item.name)}
            >
              <ListItemIcon className={classes.listItemIcon}>
                {item.checked ? <CheckedIcon /> : <UncheckedIcon />}
              </ListItemIcon>
              <ListItemText primary={item.name} />
            </ListItem>
          ))}
        </List>
      </Popover>
    </div>
  )
}
