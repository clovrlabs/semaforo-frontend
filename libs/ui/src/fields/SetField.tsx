// Set field.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import _ from 'lodash'
import clsx from 'clsx'
import { FormControl, InputLabel } from '@material-ui/core'
import { makeStyles } from '@material-ui/core/styles'
import ChipInput, { Props } from 'material-ui-chip-input'

const useStyles = makeStyles(theme => ({
  label: {
    top: theme.spacing(-1.25)
  },
  upLabel: {
    top: theme.spacing(-2)
  },
  blurLabel: {
    color: `${theme.palette.text.secondary} !important`
  }
}))

export type SetFieldProps = Omit<Props, 'value' | 'onChange' | 'allowDuplicates'> & {
  value: React.ReactText[]
  onChange: (value: React.ReactText[]) => void
  caseSensitive?: boolean
}

export default ({
  value,
  required,
  label,
  placeholder = 'Press [ , ] to enter items',
  caseSensitive = false,
  newChipKeys = [','],
  alwaysShowPlaceholder = true,
  fullWidth = true,
  margin = 'normal',
  variant,
  onChange,
  ...rest
}: SetFieldProps) => {
  const classes = useStyles()
  const [focused, setFocused] = React.useState(false)

  const equal = (v1: React.ReactText, v2: React.ReactText) => {
    const [a, b] = [`${v1}`, `${v2}`]
    return caseSensitive ? a == b : _.toLower(a) == _.toLower(b)
  }

  const removeDuplicates = (items: React.ReactText[]) =>
    items.filter((x, i) => !items.slice(i + 1).some(y => equal(x, y)))

  const onAddValue = (v: React.ReactText) => onChange && onChange(removeDuplicates([...value, v]))

  const onDeleteValue = (v: React.ReactText) =>
    onChange && onChange(value.filter(text => !equal(text, v)))

  return (
    <FormControl
      focused={focused || value.length > 0}
      fullWidth={fullWidth}
      margin={margin}
      onBlur={() => setFocused(false)}
      onFocus={() => setFocused(true)}
    >
      {label && (
        <InputLabel
          className={clsx([
            classes.label,
            { [classes.upLabel]: focused || value.length > 0 },
            { [classes.blurLabel]: !focused && value.length > 0 }
          ])}
          required={required}
        >
          {label}
        </InputLabel>
      )}
      <ChipInput
        allowDuplicates
        fullWidth={fullWidth}
        newChipKeys={newChipKeys}
        alwaysShowPlaceholder={alwaysShowPlaceholder}
        value={_.filter(value)}
        onAdd={onAddValue}
        onDelete={onDeleteValue}
        placeholder={!label || focused || value.length > 0 ? placeholder : undefined}
        variant={variant}
        {...rest}
      />
    </FormControl>
  )
}
