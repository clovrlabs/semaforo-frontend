// Text field.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import {
  FormControl,
  FormControlProps,
  FormHelperText,
  InputLabel,
  Input,
  InputProps,
  InputAdornment
} from '@material-ui/core'
import React from 'react'

export type TextFieldProps = Omit<InputProps, 'margin' | 'value' | 'onChange' | 'onBlur'> &
  Pick<FormControlProps, 'margin'> & {
    value: string
    onChange?: (value: string) => void
    errorText?: React.ReactNode
    label?: string
    helperText?: string
    endButton?: React.ReactNode
  }

// TODO: (all) add errorText
export default ({
  required,
  label,
  value,
  onChange,
  errorText,
  error,
  helperText,
  fullWidth = true,
  margin = 'normal',
  endButton,
  ...rest
}: TextFieldProps) => {
  const text = errorText || helperText

  return (
    <FormControl error={!!errorText || error} fullWidth={fullWidth} margin={margin}>
      {label && <InputLabel required={required}>{label}</InputLabel>}
      <Input
        value={value}
        onChange={e => onChange && onChange(e.target.value)}
        {...(endButton && {
          endAdornment: <InputAdornment position="end">{endButton}</InputAdornment>
        })}
        {...rest}
      />
      {text && <FormHelperText>{text}</FormHelperText>}
    </FormControl>
  )
}
