// Fields stories.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import _ from 'lodash'
import React from 'react'
import { Button, Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core'
import { SelectField, TextArea, TextField, SetField, DropdownSetField } from './index'

export default {
  title: 'Forms',
  component: SelectField
}

export const Fields = () => {
  const [items1, setItems1] = React.useState<React.ReactText[]>([])
  const [items2, setItems2] = React.useState<React.ReactText[]>(['Option 1', 'Option 2'])
  const [value, setValue] = React.useState('')
  const [selectedId, setSelectedId] = React.useState('1')

  return (
    <>
      <Dialog open fullWidth maxWidth="sm">
        <DialogTitle>Fields</DialogTitle>
        <DialogContent>
          <SetField label="Set Field" value={items1} onChange={setItems1} />
          <DropdownSetField
            label="Dropdown Set Field"
            value={items2}
            onChange={setItems2}
            options={_.range(10).map(i => `Option ${i}`)}
          />
          <SelectField
            label="Select Field"
            value={selectedId}
            onChange={setSelectedId}
            options={_.range(10).map(i => ({ value: `${i}`, label: `Option ${i + 1}` }))}
          />
          <TextField label="Text Field" />
          <TextArea value={value} onChange={setValue} />
        </DialogContent>
        <DialogActions>
          <Button>Cancel</Button>
          <Button>Continue</Button>
        </DialogActions>
      </Dialog>
    </>
  )
}
