// Select field.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import {
  Select,
  SelectProps,
  FormControl,
  FormControlProps,
  InputLabel,
  MenuItem
} from '@material-ui/core'

export type SelectFieldProps = Omit<SelectProps, 'onChange' | 'margin'> &
  Pick<FormControlProps, 'margin'> & {
    label?: string
    options: { label: string; value: string }[]
    onChange?: (value: string) => void
  }

export default ({
  label,
  value,
  required,
  options,
  onChange,
  fullWidth = true,
  margin = 'normal',
  ...rest
}: SelectFieldProps) => (
  <FormControl fullWidth={fullWidth} margin={margin}>
    {label && <InputLabel required={required}>{label}</InputLabel>}
    <Select value={value} onChange={e => onChange && onChange(`${e.target.value}`)} {...rest}>
      <MenuItem value="">&nbsp;</MenuItem>
      {options.map(({ value, label }, key) => (
        <MenuItem key={key} value={value}>
          {label}
        </MenuItem>
      ))}
    </Select>
  </FormControl>
)
