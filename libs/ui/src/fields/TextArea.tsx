// Text area field.
//
// Copyright (C) 2020 Clovrlabs SL
//
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

import React from 'react'
import SunEditor from 'suneditor-react'
import 'suneditor/dist/css/suneditor.min.css'

export type TextAreaProps = {
  value: string
  buttonList?: string[][]
  shortcutsDisable?: string[]
  imageUploadSizeLimit?: number
  imageUploadUrl?: string
  onChange?: (value: string) => void
}

export default ({
  value,
  buttonList = [
    ['undo', 'redo'],
    ['bold', 'italic', 'link', 'image']
  ],
  shortcutsDisable = ['strike', 'undo', 'indent'],
  imageUploadSizeLimit = 51200,
  imageUploadUrl = '',
  onChange
}: TextAreaProps) => {
  const editorOptions = {
    buttonList,
    shortcutsDisable,
    imageUploadSizeLimit,
    imageUploadUrl
  }

  return (
    <SunEditor
      setContents={value}
      height={250}
      onBlur={(e: any) => onChange && onChange(e.currentTarget.innerHTML)}
      setOptions={editorOptions}
    />
  )
}
