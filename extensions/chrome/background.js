// Background script
//
// Copyright (C) 2020 Clovrlabs SL
// 
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

const LOADING_STATE = 'loading'
const LISTED_STATE = 'listed'
const UNLISTED_STATE = 'unlisted'
const DISABLED_STATE = 'disabled'

let currentRequest = null;

const isUrlValid = (url) => url && url.match(/^http/)

const setTabIcon = (tabId, status) => {
  chrome.browserAction.setIcon({
    tabId,
    path: {
      '16': `assets/images/${status}_16.png`,
      '48': `assets/images/${status}_48.png`,
      '128': `assets/images/${status}_128.png`
    }
  })
}

const checkUrl = (tabId, url) => {
  if (!url) {
    return
  }

  if (!isUrlValid(url)) {
    setTabIcon(tabId, DISABLED_STATE)
    return
  }

  if (currentRequest) {
    currentRequest.abort()
  }

  chrome.storage.sync.get('apiKey', ({ apiKey }) => {
    const info = parseApiKey(apiKey)
    if (!info) {
      setTabIcon(tabId, DISABLED_STATE)
      return
    }

    // TODO: don't send the full URL (only domain and subdomain)
    const { checker, token } = info
    setTabIcon(tabId, LOADING_STATE)
    currentRequest = $.get(checker, { token, url })
      .done(data => setTabIcon(tabId, data && data.listed ? LISTED_STATE : UNLISTED_STATE))
      .fail(() => setTabIcon(tabId, UNLISTED_STATE))
  })
}

chrome.tabs.onUpdated.addListener((tabId, { url }) => {
  checkUrl(tabId, url)
})

chrome.tabs.onActivated.addListener(({ tabId }) => {
  chrome.tabs.get(tabId, ({ url }) => checkUrl(tabId, url))
})
