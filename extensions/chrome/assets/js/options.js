// Option page script
//
// Copyright (C) 2020 Clovrlabs SL
// 
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

$(() => {
  $('#title').text(chrome.i18n.getMessage('options_title'))
  $('#instructions').text(chrome.i18n.getMessage('options_instructions'))
  $('#apiKey_label').text(chrome.i18n.getMessage('options_apiKey'))

  const apiKeyField = $('#apiKey')
  const saveBtn = $('#save')
  const message = $('#message')

  const showErrorMsg = (msg) => {
    message.attr('class', 'error').text(msg)
  }

  const showSuccessMsg = (msg) => {
    message.attr('class', 'success').text(msg)
  }

  apiKeyField.on('input', () => {
    message.text('')
  })

  saveBtn.on('click', () => {
    const apiKey = apiKeyField.val().trim()

    let info = parseApiKey(apiKey)
    if (!info) {
      showErrorMsg(chrome.i18n.getMessage('options_invalidApiKey'))
      return
    }

    chrome.storage.sync.set({ apiKey }, () => {
      showSuccessMsg(chrome.i18n.getMessage('options_success'))
    })
  })
})
