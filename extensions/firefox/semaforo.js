// Background script.
//
// Copyright (C) 2020 Clovrlabs SL
// 
// This program is free software: you can redistribute it
// and/or modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or (at your
// option) any later version.
//
// This program is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
// PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with
// this program. If not, see https://www.gnu.org/licenses/.

document.body.style.border = "5px solid red";

var fqdn = window.location.href;

var request = new XMLHttpRequest();
request.open("POST", "http://127.0.0.1:5000/");
request.setRequestHeader("Content-Type", "application/json");
request.overrideMimeType("text/plain");
request.onload = function()
{
    alert("Response received: " + request.responseText);
};
request.send('{"fqdn":' + fqdn + ' }');
